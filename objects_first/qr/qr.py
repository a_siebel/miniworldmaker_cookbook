import qrcode
from miniworldmaker import *
img = qrcode.make('text')
img.save("tmp.png")
board = Board(200, 200)
@board.register
def on_setup(self):
    board.add_background("tmp.png")
    text = Text((0,180), "text")
    text.color = (0,0,0)
    text.font_size = 10
    text.x = (board.width - text.width) / 2
    board.screenshot("output.png")

board.run()
