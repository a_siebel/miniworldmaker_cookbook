from miniworldmaker import *
import random
board = Board(300, 600)
aircraft = Token()
aircraft.x = 150
aircraft.y = 500
aircraft.add_costume("images/ship.png")
enemies = []
downtime = 0

@aircraft.register
def act(self):
    global downtime
    downtime+= 1
    
@aircraft.register
def on_key_pressed(self, keys):
    if 'a' in keys:
        aircraft.x -= 1
    elif 'd' in keys:
        aircraft.x += 1

@aircraft.register
def on_key_down(self, keys):
    global downtime
    if 'SPACE' in keys and downtime > 100:
        position = aircraft.center
        position = (aircraft.center[0], aircraft.center[1] - 20)
        circle = Circle(position)
        downtime = 0
        @circle.register
        def act(self):
            self.y = self.y-1
        @circle.register
        def on_detecting_token(self, other):
            global enemies
            if other in enemies:
                other.remove()
                self.remove()
        
@board.register
def act(self):
    global enemies
    if self.frame % 120 == 0:
        enemy = Token((random.randint(30,270), 50))
        enemy.add_costume("images/enemy.png")
        enemy.orientation = 180
        enemies.append(enemy)
        @enemy.register
        def act(self):
            self.y = self.y+1
board.run()