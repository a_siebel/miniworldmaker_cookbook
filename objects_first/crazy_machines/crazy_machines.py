from miniworldmaker import *
import random
board = PhysicsBoard(400, 400)
toolbar = board.add_container(Toolbar(), "right", size=200)
lines_counter = toolbar.add_widget(CounterLabel("Lines"))

obj = "line"
state = None
line_dummy = None
max_length = 100

start = Circle((50,50))
start.physics.simulation = None

end = Circle((250,300))
end.physics.simulation = None

@end.register
def on_detecting_token(self, other):
    print("sensing token")
    if other == start:
        board.is_running = False

@board.register
def on_key_down_space(self):
    actor = Circle(start.center)
    @actor.register
    def act(self):
        if self.y > 400:
            self.board.stop()
            Text((100,100), "You loose!")
    @actor.register
    def on_detecting(self, other):
        print("sensing")
        if other == end:
            self.board.stop()
            Text((100, 100), "You won!")
    
@board.register
def on_mouse_left(self, mouse_pos):
    global state
    global obj
    if obj == "line" and not state:
        state = mouse_pos
    elif state and state.distance_to(mouse_pos) < 100 and lines_counter.get_value()<3:
        # Add the Line
        line = Line(state, mouse_pos)
        state = None
        lines_counter.add(1)
        
@board.register
def on_mouse_motion(self, mouse_pos):
    global line_dummy
    if line_dummy:
        line_dummy.remove()
    if state and state.distance_to(mouse_pos) < 100 and lines_counter.get_value()<3:
        line_dummy = Line(state, mouse_pos)
        line_dummy.border_color = (100,100,100,100)

board.run()