from miniworldmaker import *
import random

def add_planet(position, color):
    global planets
    planet = Circle(position,5)
    planet.color = color
    planets.append(planet)
    @planet.register
    def act(self):
        v = Vector.from_tokens(sun, planet)
        n = v.get_normal()
        n.normalize()
        self.move_vector(n)
        
board = Board(400,400)
sun = Circle((200,200),10)
sun.color = (255,255,0)

planets = []
for i in range(50):
    pos = (random.randint(50,350), random.randint(50,350))
    color = (random.randint(0,255),random.randint(0,255), random.randint(0,255))
    add_planet(pos, color )
    

    
board.run()