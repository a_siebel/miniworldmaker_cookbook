import miniworldmaker
board = miniworldmaker.Board(400,400)
board.gravity = (0, 0)
p1 = miniworldmaker.Token(100,100)
# @p1.register
# def on_key_pressed_space(self):
#    print("space")
    
@p1.register
def on_key_pressed_w(self):
    self.move(2)
@p1.register
def on_key_pressed_a(self):
    self.turn_left(1)
@p1.register
def on_key_pressed_d(self):
    self.turn_right(1)
        
p2 = miniworldmaker.Circle(300,100)

@p2.register
def on_key_pressed_u(self):
    self.move(2)
@p2.register
def on_key_pressed_h(self):
    self.turn_left(1)
@p2.register
def on_key_pressed_k(self):
    self.turn_right(1)
board.run()