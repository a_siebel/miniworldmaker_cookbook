from miniworldmaker import *
import random
import highscore_model

board = Board(200,600)
enemies = []
player = Circle(100,500)
player.radius = 20
my_score = 0
score_token = Number(10,10)


@player.register
def on_key_pressed_a(self):
    self.move_left()
    
@player.register
def on_key_pressed_d(self):
    self.move_right()

@player.register
def on_detecting_right_border(self):
    self.move_back()

@player.register
def on_detecting_left_border(self):
    self.move_back()
    
def create_enemy():
    enemy = Circle(random.randint(20,180), 50)
    enemy.radius = random.randint(10,30)
    enemies.append(enemy)

def new_highscore(name, points):
    highscore = highscore_model.Highscore.from_db()
    highscore.create(name, points)
    scores = highscore.from_db().scores
    for index, ranking in enumerate(scores[0:10]):
        t = Text((20, index * 40))
        t.text = f"{ranking[1]} - Points: {ranking[2]}"
        t.font_size = 10
           

@board.register
def act(self):
    global my_score
    if self.frame % 100 == 0:
        create_enemy()
    for enemy in enemies:
        enemy.move_down()
        if "bottom" in enemy.sensing_borders():
            enemies.remove(enemy)
            enemy.remove()
            my_score += 1
            score_token.set_number(my_score)
        if enemy in player.detect_tokens():
            board.stop()
            board.reset()
            name = board.ask.text(f"You reached {my_score} points! Enter your name")
            new_highscore(name, my_score)
            
    
board.run()