from miniworldmaker import *

board = TiledBoard()
board.columns = 24
board.rows = 14
board.tile_size = 20
board.add_background((255,255,255,255))
board.play_music("sounds/bensound-betterdays.mp3")
board.toolbar = board.add_container(Toolbar(), "right", size = 200)
board.console = board.add_container(Console(), "bottom", size = 100)

def create_grass(pos):
        g = Token(pos)
        g.add_costume("images/grass.png")
        g.static = True 

def create_wall(pos, walls):
        w = Token(pos)
        w.add_costume("images/wall.png")
        w.static = True
        walls.append(w)
        

for i in range(board.rows):
    for j in range(board.columns):
        create_grass((j, i))

walls = []
create_wall((0, 4), walls)
create_wall((1, 4), walls)
create_wall((2, 4), walls)
create_wall((3, 4), walls)
create_wall((4, 4), walls)
create_wall((5, 4), walls)
create_wall((6, 4), walls)
create_wall((6, 0), walls)
create_wall((6, 1), walls)
create_wall((6, 3), walls)

torch = Token((10, 4))
torch.add_costume("images/torch.png")

fireplace = Token((10, 12))
costume_not_burned = fireplace.add_costume("images/fireplace_0.png")
fireplace.burning = False
costume_burned = fireplace.add_costume(["images/fireplace_1.png", "images/fireplace_2.png"])
fireplace.switch_costume(costume_not_burned)
door = Token((6, 2))
door.add_costume("images/door_closed.png")
door.closed = True
door_open = door.add_costume("images/door_open.png")
door.switch_costume(0)
player = Token((8, 2))
player.add_costume("images/knight.png")
player.costume.is_rotatable = False

inventory = []

@player.register
def on_key_down_w(self):
    player.move_up()

@player.register
def on_key_down_s(self):
    player.move_down()

@player.register
def on_key_down_a(self):
    player.move_left()

@player.register
def on_key_down_d(self):
    player.move_right()

@board.register
def on_message(self, data):
    if data == "Torch":
        found_token = player.detect_token()
        if found_token == fireplace:
            self.console.newline("You light the fireplace.")
            fireplace.burn()
            self.toolbar.remove_widget("Torch")

@player.register
def on_detecting_token(self, token):
    global door
    if (token == door):
        if door.closed:
            self.move_back()           
            message = "The door is closed - Do you want to open it?"
            reply = self.ask.choices(message, ["Yes", "No"])
            if reply == "Yes":
                door.switch_costume(door_open)
                door.closed = False
                self.board.console.newline("You opened the door.")
        else:
            self.board.console.newline("You go through the door")
    elif (token==torch):
        reply = self.ask.choices("You find a torch - Do you want to pick it up?",["Yes", "No"])
        if reply == "Yes":
            inventory.append("Torch")
            torch.remove()
            board.console.newline("You pick up the torch")
        board.toolbar.add_widget(ToolbarButton("Torch", "images/torch.png"))    
    elif (token in walls):
        self.move_back()

@fireplace.register
def burn(self):
    if self.burning == False:
        self.board.play_sound("sounds/fireplace.wav")
        self.switch_costume(costume_burned)
        self.costume.is_animated = True

@door.register
def open(self):
    if self.closed == True:
        self.switch_costume(self.costume_open)
        self.board.play_sound("rpgsounds/olddoor.wav")
        self.closed = False


board.run()
