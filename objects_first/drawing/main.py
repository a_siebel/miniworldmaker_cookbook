from miniworldmaker import *

draw_color = (0,0,0)
line_started = False

board = Board(600, 400)
color_red = Token((0,360))
color_red.color = (255,0,0)

@board.register
def on_mouse_right(self, mouse_pos):
    global draw_color
    global start_position
    print("clecked on board")
    start_position = mouse_pos
    
@board.register
def on_mouse_right_released(self, mouse_pos):
    global draw_color
    global start_position
    print("clecked on board")
    l = Line(start_position, mouse_pos)
    l.stroke_color = draw_color

@board.register
def on_mouse_motion(self, mouse_pos):
    if self.is_mouse_left_pressed():
        self.background.draw(draw_color, mouse_pos, 1, 1)

@color_red.register
def on_clicked(self, mouse_pos):
    global draw_color
    print("set color to red")
    draw_color = color_red.color
    
board.run()