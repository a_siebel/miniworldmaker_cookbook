from miniworldmaker import *

board = Board()

r1 = Rectangle((100,210),100,15)
t1 = Triangle((115,210),(175,210), (175,200))
t2 = Triangle((90,224),(100,224), (100,210))
c1 = Circle((115,240),15)
c2 = Circle((185,240),15)
axis1 = Line((115,225), (115,255))
axis2 = Line((185,225), (185,255))
shapes = [r1, t1, t2, c1, c2, axis1, axis2]

@board.register
def act(self):
    global shapes, axis1, axis2
    for shape in shapes:
        shape.x = shape.x - 1 
    axis1.direction = axis1.direction - 1
    axis2.direction = axis2.direction - 1
board.run()