from miniworldmaker import *

board = Board()

class Car():
    def __init__(self):
        self.r1 = Rectangle((100,210),100,15)
        self.t1 = Triangle((115,210),(175,210), (175,200))
        self.t2 = Triangle((90,224),(100,224), (100,210))
        self.c1 = Circle((115,240),15)
        self.c2 = Circle((185,240),15)
        self.axis1 = Line((115,225), (115,255))
        self.axis2 = Line((185,225), (185,255))
        self.shapes = [self.r1, self.t1, self.t2, self.c1, self.c2, self.axis1, self.axis2]

    def drive_left(self):
        for shape in self.shapes:
            shape.x = shape.x - 1 
        self.axis1.direction = self.axis1.direction - 1
        self.axis2.direction = self.axis2.direction - 1

car = Car()
@board.register
def act(self):
    global car
    car.drive_left()
board.run()
