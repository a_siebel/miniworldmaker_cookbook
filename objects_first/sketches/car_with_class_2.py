import math

class Point():
    def __init__(self, x, y):
        self.x = x
        self.y = y
        
p1 = Point(10, 20)
p2 = Point(20, 30)

class Rectangle:
    def __init__(self, p1, p2):
        self.p1 = p1
        self.p2 = p2
        
    def area(self):
        a = abs(self.p1.x - self.p2.x)
        b = abs(self.p1.y - self.p2.y)
        return a*b

    def test(self):
        return math.sqrt(self.area())
    
rectangle = Rectangle(p1, p2)
print(rectangle.area())
