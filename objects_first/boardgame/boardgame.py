from miniworldmaker import *
import random

board = HexBoard(9, 9)
board.toolbar = board.add_container(Toolbar(), "right", size=280)
board.console = board.add_container(Console(), "bottom", size=100)
board.tile_size = 60
center = None
active_player = 0
corners_marker = []
tiles_marker = []
dices_marker = []
background_tokens = []
markers = [[255, 0, 0, 180], [0, 255, 0, 180]]
player_colors = [[255, 0, 0, 200], [0, 255, 0, 200]]
board.console.newline(f"P{active_player + 1} Turn")
villages = [[], []]
round_number = 1
round_label = board.toolbar.add_widget(ToolbarLabel("Round 1 of 5"), "round")
board.toolbar.add_widget(ToolbarLabel("Player 1"))

resources = [dict(), dict()]
resources[0]["grass"] = 0
resources[0]["sand"] = 0
resources[0]["stone"] = 0

resources[1]["grass"] = 0
resources[1]["sand"] = 0
resources[1]["stone"] = 0

buttons = [dict(), dict()]
# buttons[0]["grass"] =
board.toolbar.add_widget(
    Button("0", "images/grass.png"), "grass0")

buttons[0]["sand"] = board.toolbar.add_widget(
    ToolbarButton("0", "images/sand.png"), "sand0")
buttons[0]["stone"] = board.toolbar.add_widget(
    ToolbarButton("0", "images/stone.png"), "stone0")
buttons[0]["total"] = board.toolbar.add_widget(
    ToolbarLabel("Total: 1*0 + 2*0 + 3*0 = 0"), "total0")
board.toolbar.add_widget(ToolbarLabel("Player 2"))
buttons[1]["grass"] = board.toolbar.add_widget(
    ToolbarButton("0", "images/grass.png"), "grass1")
buttons[1]["sand"] = board.toolbar.add_widget(
    ToolbarButton("0", "images/sand.png"), "sand1")
buttons[1]["stone"] = board.toolbar.add_widget(
    ToolbarButton("0", "images/stone.png"), "stone1")
buttons[1]["total"] = board.toolbar.add_widget(
    ToolbarLabel("Total: 1*0 + 2*0 + 3*0 = 0"), "total1")


def create_token(tile, token_type):
    token = Token(tile.position)
    if token_type == "grass":
        token.add_costume("images/grass.png")
        token.token_type = "grass"
    elif token_type == "sand":
        token.add_costume("images/sand.png")
        token.token_type = "sand"
    elif token_type == "stone":
        token.add_costume("images/stone.png")
        token.token_type = "stone"
    return token


@board.register
def on_setup(self):
    global center
    center = self.get_tile((4, 4))
    for x in range(self.columns):
        for y in range(self.rows):
            if center.position.distance((x, y)) < 3:
                bg_tile = self.get_tile((x, y))
                if bg_tile != center:
                    r = random.randint(1, 3)
                    if r == 1:
                        bg_type = "grass"
                    elif r == 2:
                        bg_type = "sand"
                    elif r == 3:
                        bg_type = "stone"
                    number = random.randint(2, 12)
                    add_background_tile(bg_tile, bg_type, number)


def add_background_tile(bg_tile, bg_type, number):
    global background_tokens
    bg_token = create_token(bg_tile, bg_type)
    bg_tile.bg = True
    bg_tile.bg_type = bg_type
    bg_tile.number = number
    number_label = Number((0, 0), number)
    number_label.size = (0.5, 0.5)
    bg_tile.add_token(number_label)
    background_tokens.append(bg_token)


@board.register
def on_mouse_motion(self, mouse_pos):
    global corners_marker
    global tiles_marker
    global markers
    global center

    for token in corners_marker:
        token.remove()
    for token in tiles_marker:
        token.remove()

    corner = HexCorner.from_pixel(mouse_pos)
    tiles_marker = []
    corners_marker = []
    if can_place_token(active_player, corner):
        for tile in corner.get_neighbour_tiles():
            if hasattr(tile, "bg") and tile.bg:
                tile_marker = tile.create_token()
                tile_marker.border = 1
                tile_marker.fill_color = (255, 255, 100, 180)
                tile_marker.marker = True
                tiles_marker.append(tile_marker)
        if corner.position.distance(center.position) < 3:
            corner_marker = corner.create_token()
            corner_marker.border = 1
            corner_marker.size = (0.4, 0.4)
            corner_marker.fill_color = markers[active_player]
            corner_marker.is_filled = True
            corner_marker.marker = True
            corners_marker.append(corner_marker)


@board.register
def on_mouse_left(self, mouse_pos):
    global colors
    global center
    global active_player
    corner = HexCorner.from_pixel(mouse_pos)
    if can_place_token(active_player, corner):
        place_token(active_player, corner)


def can_place_token(player, corner):
    global active_player
    global villages
    global round_number
    if round_number > 5:
        return False
    for village in villages[0] + villages[1]:
        if corner.position == village.position:
            return False
    for village in villages[1 - active_player]:
        if corner.position.distance(village.position) < 1:
            return False
    if corner.position.distance(center.position) < 3:
        return True
    else:
        return False


def place_token(player, corner):
    global player_colors
    global active_player
    token = corner.create_token()
    token.border = 1
    token.size = (0.2, 0.2)
    token.fill_color = player_colors[player]
    token.is_filled = True
    token.marker = False
    token.player = player
    villages[player].append(token)
    next_turn()


def next_turn():
    global active_player
    if active_player == 0:
        active_player = 1
    else:
        active_player = 0
        next_round()


def next_round():
    global round_number
    global round_label
    round_number += 1
    round_label.text = f"Round {round_number} of 5"
    if round_number > 5:
        round_label.text = f"Game ended"
    roll_dice()


def roll_dice():
    global active_player
    global round_number
    global resources
    global buttons

    d1 = random.randint(1, 6)
    d2 = random.randint(1, 6)
    dice = d1 + d2
    board.console.newline(
        f"Rolled dice: {d1} and {d2} = {dice} ...starting round {round_number}"
    )
    add_resources_to_players(dice)


def add_resources_to_players(dice):
    global dices_marker
    global background_tokens
    new_resources = [0, 0]
    for token in dices_marker:
        token.remove()
    for token in background_tokens:
        tile = board.get_tile(token.position)
        if tile.number == dice:
            for corner in tile.corners:
                tokens = corner.get_tokens()
                for token in tokens:
                    if hasattr(token, "player"):
                        new_resources[token.player] += 1
            dice_marker = tile.create_token()
            dice_marker.color = (0, 0, 0, 50)
            dices_marker.append(dice_marker)
    resources[0][tile.bg_type] += new_resources[0]
    resources[1][tile.bg_type] += new_resources[1]
    buttons[0][tile.bg_type].text = resources[0][tile.bg_type]
    buttons[1][tile.bg_type].text = resources[1][tile.bg_type]

    total_0 = resources[0][
        "grass"] + resources[0]["sand"] * 2 + resources[0]["stone"] * 3
    total_1 = resources[1][
        "grass"] + resources[1]["sand"] * 2 + resources[1]["stone"] * 3
    buttons[0][
        "total"].text = f"Total: {resources[0]['grass']}*1 + {resources[0]['sand']}*2 + {resources[0]['stone']}*3 = {total_0}"
    buttons[1][
        "total"].text = f"Total: {resources[1]['grass']}*1 + {resources[1]['sand']}*2 + {resources[1]['stone']}*3 = {total_1}"


board.run()
