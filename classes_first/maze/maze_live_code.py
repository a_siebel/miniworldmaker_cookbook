from miniworldmaker import *

board = TiledBoard(3, 3)

maze = (("", "", "w"),
        ("", "", "w"),
        ("", "", "w"))

class Wall(Token):
    def on_setup(self):
        self.add_costume("wall")
        
for i, row in enumerate(maze):
    for j, column in enumerate(row):
        if column:
            Wall(j, i)

board.run()
