from miniworldmaker import *
import sys
from random import randint, choice
import copy

# based on https://github.com/kantel/pygamezero/tree/master/tappyplane

WIDTH = 800
HEIGHT = 480
left = WIDTH/2
bottom = HEIGHT/2
bottomground = HEIGHT - 35

board = Board(WIDTH, HEIGHT)

BACKGROUND = "background"
GROUND     = "groundgrass"
no_enemies = 10
enemyships = ["shipbeige", "shipblue", "shipgreen", "shippink", "shipyellow"]

board.add_background(BACKGROUND)

ground0 = Token((left, bottomground))
ground0.add_costume(GROUND)
ground1 = Token((WIDTH + left, bottomground))
ground1.add_costume(GROUND)
grounds = [ground0, ground1]


groundlevel = HEIGHT - 85



class Plane(Token):
    def on_setup(self):
        self.add_costume("planered1")
        self.count = 0
        self.gravity = 0.1
        self.upforce = -15
        self.velocity = 0
        self.fire = False

    def act(self):
        self.velocity += self.gravity
        self.velocity *= 0.9         # Reibung/Luftwiderstand
        self.y += self.velocity
        if self.y >= groundlevel:    # Flugzeug ist am Boden
            self.y = groundlevel
            self.velocity = 0
        if self.y <= 20:             # Flugzeug ist am oberen Fensterrand
            self.y = 20
            self.velocity = 0
    
    def up(self):
        self.velocity += self.upforce

    def on_key_down_w(self):
        self.up()
        
    def on_key_down_d(self):
        if not self.fire:
            self.fire = True
            bullet = Bullet()
            @timer(frames=30)
            def downtime():
                self.fire = False

#def on_setup(self):
#    self.count += 1
#    if self.count > 9: self.count = 0
#    if self.count < 3:
#        self.add_costume("planered1")
#    elif self.count < 6:
#        self.image = "planered2"
#    else:
#        self.image = "planered3"

        
class Bullet(Token):
    def on_setup(self):
        self.add_costume("laserred")
        self.x = plane.x
        self.y = plane.y
        self.speed = 25
        self.fire = False
    
    def act(self):
        self.x += self.speed

    def on_detecting_enemy(self, enemy):
        enemy.reset()
        
    def on_not_detecting_board(self):
        self.remove()
        
class Enemy(Token):
    
    def on_setup(self):
        self.width = 44
        self.speed = -1.5

    def reset(self):
        self.x = randint(WIDTH + 50, WIDTH + 500)
        self.y = randint(25, groundlevel)    

    def act(self):
        self.x += self.speed
        if self.x <= -self.width:
            self.reset()

plane = Plane((100, HEIGHT//2))

enemies = []
for _ in range(no_enemies):
    enemy = Enemy(randint(WIDTH + 50, WIDTH + 750), randint(50, groundlevel))
    enemy.add_costume(choice(enemyships))
    enemies.append(enemy)

        
board.run()