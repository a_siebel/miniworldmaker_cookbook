from miniworldmaker import *
from random import randint, choice
import cProfile as profile

# based on https://github.com/kantel/pygamezero/tree/master/tappyplane

board = Board(800, 480)

left = board.width / 2
bottom = board.height / 2
bottomground = board.height - 35

no_enemies = 10
enemyships = ["shipbeige", "shipblue", "shipgreen", "shippink", "shipyellow"]

# Add backgrounds
back0 = Token()
back0.add_costume("background")
back0.size = board.width, board.height
back1 = Token(board.width, 0)
back1.size = board.width, board.height
back1.add_costume("background")
backs = [back0, back1]

ground0 = Token((0, bottomground))
ground0.add_costume("groundgrass")
ground0.width = board.width 
ground0.costume.is_scaled = True
ground1 = Token((board.width, bottomground))
ground1.add_costume("groundgrass")
ground1.width = board.width
ground1.costume.is_scaled = True
grounds = [ground0, ground1]


groundlevel = board.height - 85

@board.register
def act(self):
    for back in backs:
        back.x -= 1
        if back.x <= - board.width:
            back.x = board.width
    for ground in grounds:
        ground.x -= 2
        if ground.x <= - board.width:
            ground.x = board.width

class Plane(Token):
    def on_setup(self):
        self.add_costume("planered1")
        self.gravity = 0.1
        self.velocity_y = 0
        self.fire = False

    def act(self):
        self.velocity_y += self.gravity
        self.velocity_y *= 0.9 # friction
        self.y += self.velocity_y
        if self.y >= groundlevel:    
            self.y = groundlevel
            self.velocity_y = 0
        if self.y <= 20:             
            self.y = 20
            self.velocity_y = 0

    def on_key_down_w(self):
        self.velocity_y -= 5
        
    def on_key_down_d(self):
        if not self.fire:
            self.fire = True
            bullet = Bullet()
            @timer(frames=30)
            def downtime():
                self.fire = False
      
class Bullet(Token):
    def on_setup(self):
        self.add_costume("laserred")
        self.x = plane.x
        self.y = plane.y
        self.speed = 25
        self.fire = False
    
    def act(self):
        self.x += self.speed

    def on_detecting_enemy(self, enemy):
        enemy.reset()
        
    def on_not_detecting_board(self):
        self.remove()
        
class Enemy(Token):
    
    def on_setup(self):
        self.add_costume(choice(enemyships))
        self.speed = -1.5

    def reset(self):
        self.x = randint(board.width + 50, board.width + 500)
        self.y = randint(25, groundlevel)    

    def act(self):
        self.x += self.speed
        if self.x <= -self.width:
            self.reset()

plane = Plane((100, board.height / 2))

enemies = []
for _ in range(no_enemies):
    enemy = Enemy()
    enemy.reset()
    enemies.append(enemy)

profile.run('board.run()')
board.run()