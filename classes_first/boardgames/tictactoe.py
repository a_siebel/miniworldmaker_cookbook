from miniworldmaker import *
import miniworldmaker
from boardgame_state import BoardGameState
from boardgame_board import BoardGame
from boardgame_screen import BoardGameScreen
from typing import List, Tuple, Union, Optional

class X(TextToken):
    def on_setup(self):
        self.set_text("X")
        self.color = (255,255,0)
        
    def __str__(self):
        return "X"

class O(TextToken):
    def on_setup(self):
        self.set_text("O")
        self.color = (0, 255, 255)

    def __str__(self):
        return "O"


class TicTacToeState(BoardGameState):
    
    def playing_field_dimensions(self):
        return 3, 3
    
    def set_initial_state(self):
        self.add_player("X")
        self.add_player("O")
        self.free_fields = [(0,0),(0,1),(0,2),(1,0),(1,1),(1,2),(2,0),(2,1),(2,2)]
        
    def copy(self):
        cp = super().copy()
        cp.free_fields = self.free_fields
        return cp
    
    def is_valid_placement(self, target: Union[miniworldmaker.Position, tuple]):
        if self.is_free(target):
            return True
        else:
            return False
   
    def place_token(self, target):
        self.set_token(target, self.current_player)
        self.next_player()
        self.free_fields = [field for field in self.free_fields if field != target]
        
    def has_won(self, player) -> bool:
        playing_field = self.playing_field
        for index in range(3):
            if playing_field[0][index] == playing_field[1][index] == playing_field[2][index] == player:
                return True
        for index in range(3):
            if playing_field[index][0] == playing_field[index][1] == playing_field[index][2] == player:
                return True
        if playing_field[0][0] == playing_field[1][1] == playing_field[2][2] == player:
            return True
        if playing_field[0][2] == playing_field[1][1] == playing_field[2][0] == player:
            return True
        return False
    
    def is_draw(self) -> bool:
        if not self.free_fields:
            return True
        else:
            return False
           
    def get_possible_moves(self) -> List:
        moves = []
        if not self.is_game_over():
            for target in self.free_fields:
                move = self.create_move(place = target)
                moves.append(move)
        return moves


class TicTacToeBoard(BoardGame):
    
    def register_tokens(self):
        self.register_token("O", O)
        self.register_token("X", X)
        
    def settings(self):
        self.ai = True
            
    def _initial_state(self):
        return TicTacToeState()

    def screen(self):
       return BoardGameScreen()

board = TicTacToeBoard()
board.run()