from boardgame_screen import BoardGameScreen


class LastMovesScreen(BoardGameScreen):
    
    def draw(self):
        self.clear()
        state = self.board.initial_state
        if not hasattr(self.board.state, "move_list"):
            return
        for index, move in enumerate(reversed(self.board.state.move_list)):
            state = state.get_next_state(move)
        last_index = len(best_move["move_list"]) + 1
        self.boundary_y = (len(best_move["move_list"])) * 50 + 20
        self.camera.topleft = (0, 0)
    
    