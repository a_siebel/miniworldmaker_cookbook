from miniworldmaker import *
from boardgame_state import BoardGameState
from boardgame_board import BoardGame
from boardgame_screen import BoardGameScreen
from boardgame_last_moves_screen import LastMovesScreen
import math

turn = 1
marker = None

class Player(Token):
    def on_setup(self):
        self.add_costume("images/penguin.png")
        self.color = (255,255,0)
        
    def __str__(self):
        return "P"

class Croco(Token):
    def on_setup(self):
        self.add_costume("images/crocodile.png")
        self.color = (0,255,255)

    def __str__(self):
        return "C"
            
class CrocoChessState(BoardGameState):
    
    def playing_field_dimensions(self):
        return 3, 3
    
    def set_initial_state(self):
        print("set initial state for croco chess")
        self.add_player("P")
        self.add_player("C")
        self.playing_field[0][0] = "P"
        self.playing_field[1][0] = "P"
        self.playing_field[2][0] = "P"
        self.playing_field[0][2] = "C"
        self.playing_field[1][2] = "C"
        self.playing_field[2][2] = "C"

    def is_valid_movement(self, source, target):
        moves = self.get_possible_moves()
        for move in moves:
            if move["move"][0] == source and move["move"][1] == target:
                return True
        return False

    def move_token(self, source, target):
        self.unset_token(source)
        self.set_token(target, self.current_player)
        self.next_player()
    
    def has_won(self, player):
        return self.get_winner() == player
    
    def get_winner(self):
        if self.count_tokens(self.current_player) == 0:
            return self.get_next_player()
        if not self.get_possible_moves():
            return self.get_next_player()
        return None
    
    def is_draw(self):
        return False
           
    def get_possible_moves(self):
        moves = []
        sources = self.get_token_positions_by_player(self.current_player)
        for source in sources:
            if self.current_player == "P":
                target_right = (source[0] + 1, source[1] + 1)
                if self.is_from_player(target_right, "C"):
                    moves.append(self.create_move((source, target_right)))
                target_front = (source[0] , source[1] + 1)
                if self.is_free(target_front):
                    moves.append(self.create_move((source, target_front)))               
                target_left = (source[0] - 1, source[1] + 1)
                if self.is_from_player(target_left, "C"):
                    moves.append(self.create_move((source, target_left)))      
            if self.current_player == "C":
                target_right = (source[0] + 1, source[1] - 1)
                if self.is_from_player(target_right, "P"):
                    moves.append(self.create_move((source, target_right)))
                target_front = (source[0] , source[1] - 1)
                if self.is_free(target_front):
                    moves.append(self.create_move((source, target_front)))
                target_left = (source[0] - 1, source[1] - 1)
                if self.is_from_player(target_left, "P"):
                    moves.append(self.create_move((source, target_left)))
        return moves
        
class CrocoChessBoard(BoardGame):

    def register_tokens(self):
        self.register_token("P", Player)
        self.register_token("C", Croco)
        
    def settings(self):
        self.ai = False
        
    def screen(self):
        return BoardGameScreen()
        
    def _initial_state(self):
        return CrocoChessState()

    def is_valid_selection(self, pos):
        return not self.selected and self.state.is_current_player(self.state.get_token(pos))

            
board = CrocoChessBoard()
board.run()