import miniworldmaker

class BoardGameScreen(miniworldmaker.PixelBoard):
    def __init__(self):
        super().__init__(600, 100)
        self.add_background((40,40,40))
        self.board = None # injected by add_board in boardgame class
        

    def create_token(self, state, size, local_x, local_y, global_x, global_y):
        playing_field = state.playing_field
        if playing_field[local_x][local_y] in self.board.registered_tokens:
            token = self.board.get_token(playing_field[local_x][local_y])
            return self.create_token_on_local_board(token, state, size, local_x, local_y, global_x, global_y)
        
    def get_color(self, player):
        token = self.board.get_token(player)
        color = token.color
        token.remove()
        return color
    
    def create_rect(self, state, size, local_x, local_y, global_x, global_y):
        token = miniworldmaker.Rectangle()
        return self.create_token_on_local_board(token, state, size, local_x, local_y, global_x, global_y)

    def create_Number(self, number, state, size, local_x, local_y, global_x, global_y):
        token = Number()
        token.value = number
        return self.create_token_on_local_board(token, state, size, local_x, local_y, global_x, global_y)
        
    def create_token_on_local_board(self, token, state, size, local_x, local_y, global_x, global_y):
        token.board = self
        if hasattr(token, "text"):
            token.fixed_size = True
            token.font_size = size / 3
        else:
            token.size = (size / 3, size / 3)
        token.x = token.x + size / state.playing_field_width * local_x
        token.y = token.y + size / state.playing_field_height * local_y
        token.x += global_x
        token.y += global_y
        return token

    def draw(self):
        self.clear()
        state = self.board.state
        best_move = state.get_best_move()
        
        self.boundary_x = len(state.get_possible_moves()) * 50 + 20
        if not best_move or not best_move["move_list"]:
            return
        for index, move in enumerate(best_move["move_list"]):
            best_state = state.get_next_state(move)
            self.draw_possible_moves(state, 10 + index * 50, best_state)
            state = state.get_next_state(move)
        last_index = len(best_move["move_list"]) + 1
        self.draw_possible_moves(state, 10 + last_index * 50, best_state)
        self.boundary_y = (len(best_move["move_list"])) * 50 + 20
        self.camera.topleft = (0, 0)
        
        
    def draw_possible_moves(self, state, y, best_state):    
        for index, move in enumerate(state.get_possible_moves()):
            next_state = state.get_next_state(move)
            position =  (10 + index * 50, y)
            size = 30
            if next_state == best_state:
                color = self.get_color(move["player"])
            else:
                color = (100, 100, 100)
            self.draw_state(next_state, position = position, size = size, color = color)
            self.draw_move(next_state, move, position = position, size = size, color = color)

    def draw_state(self, state, position = (0, 0), size = 30, color = (100, 100, 100)):
        for i in range(state.playing_field_height):
            t1 = self.create_token(state, size, 0, i, position[0], position[1])
            t2 = self.create_token(state, size, 1, i, position[0], position[1])
            t3 = self.create_token(state, size, 2, i, position[0], position[1])
        r = miniworldmaker.Rectangle()
        r.border = 2
        r.board = self
        r.position = position            
        r.width = size
        r.height = size
        r.border_color = color           
    
    def draw_move(self, state, move, position = (0, 0), size = 30, color = (100, 100, 100)):
        if "move" in move and move["move"]:
            rect = self.create_rect(state, size, move["move"][0][0], move["move"][0][1], position[0], position[1])
            rect = self.create_rect(state, size, move["move"][1][0], move["move"][1][1], position[0], position[1])
            color = self.get_color(move["player"])
        elif "place" in move and move["place"]:
            rect = self.create_token(state, size, move["place"][0], move["place"][1], position[0], position[1])
            color = self.get_color(move["player"])
        pass
    
    def on_key_pressed_s(self):
        self.camera.y += 1
        
    def on_key_pressed_w(self):
        self.camera.y -= 1
        
    def on_key_pressed_a(self):
        self.camera.x -= 1
        
    def on_key_pressed_d(self):
        self.camera.x += 1