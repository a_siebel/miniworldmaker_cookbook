from abc import ABCMeta, abstractmethod
import copy
import math
import miniworldmaker
from typing import Union, List, Dict

class BoardGameState():

    def __init__(self):
        self.playing_field_width, self.playing_field_height = self.playing_field_dimensions()
        self.playing_field = list()
        self.players = list()
        self.tokens = list()
        self.current_player = None
        self.last_moves = list()
        self._setup_playing_field()
        self.set_initial_state()

    def set_initial_state(self):
        pass
    
    def _setup_playing_field(self):
        for column in range(self.playing_field_width):
            self.playing_field.append(list())
            for row in range(self.playing_field_height):
                self.playing_field[column].append(row)
                self.playing_field[column][row] = ""
            
    def register_token(self, letter):
        self.tokens.append(letter)
        
    def add_player(self, player):
        self.players.append(player)
        if self.current_player is None:
            self.current_player = player

    def get_token_positions_by_player(self, player):
        positions = []
        for row in range(len(self.playing_field)):
            for column in range(len(self.playing_field[row])):
                if self.playing_field[column][row] == player:
                    positions.append((column, row))
        return positions
                
    def create_move(self, move = None, place = None, player = None) -> dict:
        if player == None:
            player = self.current_player
        mv = dict()
        mv["player"] = player
        mv["move"] = move
        mv["place"] = place
        return mv
        
    def next_player(self):
        self.current_player = self.get_next_player()

    def is_free(self, pos : Union[miniworldmaker.Position, tuple]) -> bool:
        if not self.is_on_the_board(pos):
            return False
        return self.playing_field[pos[0]][pos[1]] == ""

    def is_from_player(self, pos, player):
        if not self.is_on_the_board(pos):
            return False
        return self.playing_field[pos[0]][pos[1]] == player
    
    def count_tokens(self, player):
        count = 0
        for i in range(self.playing_field_width):
            for j in range(self.playing_field_height):
                if self.playing_field[i][j] == player:
                    count += 1
        return count

    def unset_token(self, pos):
        self.playing_field[pos[0]][pos[1]] = ""
    
    def set_token(self, pos, player):
        self.playing_field[pos[0]][pos[1]] = player
        
    def get_token(self, pos):
        return self.playing_field[pos[0]][pos[1]] 

    def is_on_the_board(self, pos: Union[miniworldmaker.Position, tuple]):
        print(pos)
        if pos[0] < 0 or pos[1] < 0:
            return False
        if pos[0] >= self.playing_field_width:
            return False
        if pos[1] >= self.playing_field_height:
            return False
        return True
        
    def is_current_player(self, player):
        return player == self.current_player
    
    def get_next_player(self):
        index = self.players.index(self.current_player)
        if index < len(self.players) - 1:
            return self.players[index + 1]
        else:
            return self.players[0]
            
    def is_valid_movement(self, target):
        return False
    
    def is_valid_placement(self, target):
        return False
    
    def is_valid_move(self, move):
        if not self.is_current_player(move["player"]):
            return False
        if "move" in move and move["move"]:
            source = move["move"][0]
            target = move["move"][1]
            if self.is_game_over():
                return False
            if not self.is_on_the_board(target):
                return False        
            return self.is_valid_movement(source, target)
        elif "place" in move and move["place"]:
            target = move["place"]
            if self.is_game_over():
                return False
            if not self.is_on_the_board(target):
                return False
            return self.is_valid_placement(target)
        else:
            raise Exception("Move should contain 'place' or 'move'")

    def move(self, move):
        """Checks if action is a valid game move

        move["place"]: tuple with position where token should be placed.
        move["move"]: tuple with source and target
        move["player"]: player, who does the move
        """
        if "move" in move and move["move"]:
            source = move["move"][0]
            target = move["move"][1]
            self.move_token(source, target)
        elif "place" in move and move["place"]:
            target = move["place"]
            self.place_token(target)
        else:
            raise Exception("Move should contain 'place' or 'move'")

    def move_token(source, target):
        pass
    
    def place_token(source, target):
        pass
    
    @abstractmethod    
    def get_possible_moves(self):
        pass
    
    def is_game_over(self):
        for player in self.players:
            if self.has_won(player) or self.is_draw():
                return True
        return False
        
    def __str__(self):
        result = ("STATE:" + "\n")
        for i in range(len(self.playing_field)):
                result += "_" + str(self.playing_field[0][i]) + "_" + str(self.playing_field[1][i]) + "_" + str(self.playing_field[2][i]) + "_" + "\n"
        return result
    
    def __eq__(self, other):
        if self.__class__ != other.__class__:
            return False
        if self.current_player != other.current_player:
            return False
        if self.playing_field != other.playing_field:
            return False
        return True

    def copy(self):
        a = self.__class__()
        a.playing_field = copy.deepcopy(self.playing_field)
        a.current_player = self.current_player
        a.players = self.players
        a.tokens = self.tokens
        a.last_moves = self.last_moves.copy()
        return a
       
    @classmethod
    def create_from_move(cls, state, move):
        target_state = state.copy()
        target_state.move(move)
        return target_state
    
    def get_next_state(self, move):
        return self.__class__.create_from_move(self, move)
       
    def get_best_move(self):
        best_score = -math.inf
        best_move = None
        best_endstate = None
        for move in self.get_possible_moves():
            next_state = self.get_next_state(move)
            next_state.move_list = [move]
            score, final_state = self.minimax_search(next_state, 0, self.current_player, maxdepth = 10, alpha = - math.inf, beta = math.inf)
            if (score > best_score):
                best_score = score
                best_move = move
                best_move["final_state"] = final_state
                if final_state:
                    best_move["move_list"] = final_state.move_list
        return best_move
    
    @classmethod
    def minimax_search(cls, state, depth, maximizer, maxdepth, alpha, beta):
        if state.is_game_over():
            if (state.is_draw()):
                return 0, state
            elif (state.has_won(maximizer)):
                return 1, state
            else:
                return -1, state
        if depth > maxdepth:
            return 0, state
        # Here the recursion part begins...
        if state.current_player == maximizer:
            best_value = - math.inf
        else:
            best_value = math.inf
        final_state = None
        for move in state.get_possible_moves():
            next_state = cls.create_from_move(state, move)
            next_state.last_move = move
            next_state.last_state = state
            next_state.move_list = state.move_list + [move]
            value, last_state = cls.minimax_search(next_state, depth + 1, maximizer, maxdepth, alpha, beta)
            if state.current_player == maximizer and value > best_value:
                best_value = value
                alpha = max(alpha, best_value)
                best_move = move
                final_state = last_state
                if beta <= alpha:
                    break
            if state.current_player != maximizer and value < best_value:
                best_value = value
                beta = min(beta, best_value)
                if beta <= alpha:
                    break
                best_move = move
                final_state = last_state
        return best_value, final_state