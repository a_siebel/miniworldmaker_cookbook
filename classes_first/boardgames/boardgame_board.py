from abc import ABCMeta, abstractmethod
import miniworldmaker
from miniworldmaker import Tile, Position
from boardgame_screen import BoardGameScreen
from typing import Union

class Marker(miniworldmaker.Token):
    def on_setup(self):
        self.add_costume((255,255,255,20))
    def on_mouse_leave(self, pos):
        self.remove()
        
class SelectionMarker(miniworldmaker.Token):
    def on_setup(self):
        self.add_costume((255,0,255,100))
        
class BoardGame(miniworldmaker.TiledBoard, metaclass=ABCMeta):
    def __init__(self):
        self.state = self._initial_state()
        print("init board")
        super().__init__()
        self.columns = self.state.playing_field_width
        self.rows = self.state.playing_field_height
        self.registered_tokens = dict()
        self.boardgame_screen = None
        self.selected = None
        self.selection_enabled = True
        self.selection_marker = None
        self.ai = True
        self.initial_state = None
        self.register_tokens()
        self.settings()
        self.add_screen(self.screen())
        self.update_from_current_state()
        
    def register_tokens(self):
        pass

    def _initial_state(self):
        pass
    
    def on_setup(self):
        self.update_from_current_state()

    def select(self, pos):
        self.selected = pos
        self.selection_marker = SelectionMarker()
        self.selection_marker.position = pos
        
    def unselect(self):
        self.selected = None
        if self.selection_marker:
            self.selection_marker.remove()
            self.selection_marker = None
        
    def add_player(self, player):
        self.state.add_player()
        
    def register_token(self, letter, token_class):
        self.state.register_token(letter)
        self.registered_tokens[letter] = token_class
        
    def get_token(self, letter):
        if letter in self.registered_tokens:
            return  self.registered_tokens[letter]()
        
    def detect_game_tokens(self, position):
        """Detect all X's and O's but don't selfdetect marker
        """
        tokens = self.detect_tokens(position)
        return [token for token in tokens if not isinstance(token, Marker)]
    
    def on_mouse_motion(self, pos):
        tile =Tile.from_pixel(pos)    
        if (self.selected and self.is_valid_movement(self.selected, tile.position)) or self.is_valid_placement(tile.position) or self.is_valid_selection(tile.position):
            x = Marker(tile)
            
    def is_valid_selection(self, pos):
        pass
    
    def is_valid_placement(self, pos: Union[miniworldmaker.Position, tuple]):
        move = self.state.create_move(place = pos)
        return self.state.is_valid_placement(pos)
        
    def is_valid_movement(self, source, target):
        if not self.selected:
            return False
        move = self.state.create_move(move = (source, target))
        return self.state.is_valid_move(move)
    
    def place(self, position):
        move = self.state.create_move(place = position)
        if self.state.is_valid_move(move):
            self.state.move(move)
            success = True
        else:
            raise Exception("Tried to execute non valid placement", data)
        
    def move(self, source, target):
        move = self.state.create_move(move = (source, target))
        if self.state.is_valid_move(move):
            self.state.move(move)
        else:
            raise Exception("Tried to execute non valid movement")

    def on_mouse_left(self, pos):
        print("mouse left", self.selection_enabled)
        tile =Tile.from_pixel(pos)
        moved = False
        print("valid movement", self.selected, self.is_valid_movement(self.selected, tile.position))
        if self.selection_enabled:
            if not self.selected and self.is_valid_selection(tile.position):
                print("select")
                self.select(tile.position)
            elif not self.selected and self.is_valid_placement(tile.position):
                print("place")
                self.place(tile.position)
                self.unselect()
                self.update_from_current_state()
            elif self.selected and self.is_valid_movement(self.selected, tile.position):
                print("move")
                self.move(self.selected, tile.position)
                self.unselect()
                self.update_from_current_state()
                moved = True
            elif self.selected and not self.is_valid_movement(self.selected, tile.position):
                self.unselect()
        if not self.selection_enabled:
            if self.is_valid_placement(tile.position):
                self.place(tile.position)
                self.update_from_current_state()
                moved = True
        # post processing
        if moved:
            if self.boardgame_screen:
                self.boardgame_screen.draw()
            if self.ai:
                best_move = self.state.get_best_move()
                print(best_move)
                if self.state.is_valid_move(best_move):
                    self.state.move(best_move)
            
    def update_from_current_state(self):
        """Updates board from current state."""
        self.clear()
        for row in range(len(self.state.playing_field)):
            for column in range(len(self.state.playing_field[row])):
                letter = self.state.playing_field[row][column]
                if letter is not None and letter != "":
                    token = self.registered_tokens[letter](row, column)
                    token.set_board(self)
    
    def add_screen(self, screen):
        if not self.boardgame_screen:
            self.boardgame_screen = screen
            self.boardgame_screen.board = self
            self.add_board("right", self.boardgame_screen)
            self.boardgame_screen.draw()