from miniworldmaker import *
from objects import *
from levels import *
from level_manager import *


level_manager = LevelManager()
board = level_manager.get_current()
board.run()