from miniworldmaker import *
from levels import *

class LevelManager:
    def __init__(self):
        """LevelFelix1(self), LevelBruno1(self),"""
        self.levels = [LevelBruno1(self), Level2(self), Level3(self), Level4(self), Level5(self), Level6(self), Level7(self), Level8(self), LevelEnd(self)]
        self.actual_level = 0

    def next_level(self):
        next_level = self.actual_level + 1
        if next_level < len(self.levels):
            self.get_current().switch_board(self.levels[next_level])
            self.actual_level  = next_level
    
    def first_level(self):
        self.get_current().switch_board(self.levels[0])
        self.actual_level  = 0

    def get_current(self):
        return self.levels[self.actual_level]
    
    def same_level(self):
        self.get_current().switch_board(self.levels[self.actual_level])