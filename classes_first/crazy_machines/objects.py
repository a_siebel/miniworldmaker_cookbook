from miniworldmaker import *

class Selector(CounterLabel):
    def __init__(self, name):
        super().__init__(name)
        self.obj = None
        self.board = None
        
    def on_mouse_left(self, mouse_pos):
        self.board.select_object(self.obj)
        
    def on_message(self, text):
        print("received message", text)
        
    def add_to_object(self, obj):
        self.obj = obj
        self.board = obj.board
        
class Object():
    def __init__(self, name):
        self.state = None
        self.name = name
        self.counter = Selector(name)
        self.counter.set(1)
        self.max_objects = 1
        self.board = None
        self.dummy = None
        self.has_state = True
        
    def on_click(self, mouse_pos):
        if self.has_state and not self.state and self.board.is_position_on_board(mouse_pos):
            self.state = mouse_pos
        elif self.has_state and self.state and self.valid_placement(mouse_pos) and self.board.is_position_on_board(mouse_pos):
            self.create_object(self.state, mouse_pos)
            self.counter.sub(1)
        elif not self.has_state and self.valid_placement(mouse_pos) and self.board.is_position_on_board(mouse_pos):
            self.create_object(self.state, mouse_pos)
            self.counter.sub(1)

    def create_dummy(self, state, mouse_pos):
        obj = self.create_token(state, mouse_pos)
        obj.border_color = (200,200,200,150)
        obj.color = (200,200,200,150)
        obj.physics.simulation = None
        return obj
    
    def create_token(self, state, mouse_pos):
        pass
        
    def create_object(self, state, mouse_pos):
        if self.dummy:
            self.dummy.remove()
        token = self.create_token(state, mouse_pos)
        self.state = None
        return token
        
            
    def valid_placement(self):
        pass
        
    def add_to_level(self, level, max_objects):
        level.toolbar.add_widget(self.counter)
        level.objects.append(self)
        self.max_objects = max_objects
        self.counter.set(self.max_objects)
        self.board = level
        self.counter.add_to_object(self)
        
    def on_motion(self, mouse_pos):
        if self.dummy:
            self.dummy.remove()
        if self.has_state and self.state and self.valid_placement(mouse_pos):
            self.dummy = self.create_dummy(self.state, mouse_pos)
        elif not self.has_state and self.valid_placement(mouse_pos):
            self.dummy = self.create_dummy(self.state, mouse_pos)



class LineObject(Object):
    
    def __init__(self, max_length):
        super().__init__("Line")
        self.max_length = max_length
        
    def valid_placement(self, mouse_pos):
        return self.state and self.state.distance_to(mouse_pos) < self.max_length and self.counter.get_value() >= 0
        
    def create_token(self, state, mouse_pos):
        return Line(self.state, mouse_pos)
        
class RectObject(Object):
    
    def __init__(self, max_length):
        super().__init__("Rectangle")
        self.max_length = max_length
        
    def create_token(self, state, mouse_pos):
        return Rectangle(self.state, mouse_pos[0]-self.state[0], mouse_pos[1]-self.state[1])
        
    def valid_placement(self, mouse_pos):
        return self.state and self.state.distance_to(mouse_pos) < self.max_length and self.counter.get_value() > 0 and mouse_pos[0]-self.state[0] > 0 and mouse_pos[1]-self.state[1] > 0

class PinJointObject(Object):
    
    def __init__(self, max_length, anchor):
        super().__init__("Linked circle")
        self.max_length = max_length
        self.anchor = anchor
        self.has_state = False
        
    def create_token(self, state, mouse_pos):
        c = Circle(mouse_pos, 10)
        return c
        
    def create_object(self, state, mouse_pos):
        c = super().create_object(state, mouse_pos)
        c.physics.join(self.anchor)
        self.board.connect(c, self.anchor)
        
    def valid_placement(self, mouse_pos):
        return mouse_pos.distance_to(self.anchor.position) and mouse_pos.distance_to(self.anchor.position) < 100 and self.counter.get_value() > 0

class Cloud(Token):
    def __init__(self, position):
        super().__init__(position)
        
    def on_setup(self):
        self.size = 40
        self.physics.simulation = "manual"
        self.add_costume("images/cloud.png")
        self.costume.is_scaled = True
        self.center = self.position
    
    def on_touching_token(self, other, info):
        other.impulse(0,1500)

class Cloud(Token):
    def __init__(self, position):
        super().__init__(position)
        
    def on_setup(self):
        self.size = 40
        self.physics.simulation = "manual"
        self.add_costume("images/cloud.png")
        self.costume.is_scaled = True
        self.center = self.position
    
    def on_touching_token(self, other, info):
        print("touching", other)
        other.impulse(0,1500)
        

class CloudObject(Object):
    
    def __init__(self, max_length):
        super().__init__("Cloud")
        self.max_length = max_length
        self.has_state = False
        
    def create_token(self, state, mouse_pos):
        t = Cloud(mouse_pos)
        return t
        
    def create_object(self, state, mouse_pos):
        c = super().create_object(state, mouse_pos)
        
    def valid_placement(self, mouse_pos):
        return self.counter.get_value() > 0

class Trap(Token):
    
    def __init__(self, position, trap):
        super().__init__(position)
        self.trap = trap
        self.size = 10
        self.color = (0,0,0)
        
    def on_setup(self):
        self.physics.simulation = None
        
    def on_detecting_token(self, other):
        self.remove()
        self.trap.remove()

class Wheel:
    def __init__(self,position, size, count):
        for i in range(count):
            line = Line((position[0], position[1] + size/2), (position[0], position[1] - size/2))
            line.direction = 0 + ((180/count) * i)
            line.border_color = (255,255,360 /count)
            @line.register
            def act(self):
                self.direction += 1
class Marble(Circle):
    def on_setup(self):
        self.color = (80,160,180)
        self.physics.elasticity = 0.2
    
class Base(Line):
    def on_setup(self):
        self.thickness = 1