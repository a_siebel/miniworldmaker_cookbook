from miniworldmaker import *

class Wall(Token):
    def on_setup(self):
        self.add_costume((255,255,0))

class Door(Token):
    def on_setup(self):
        self.add_costume((255,0,255))
        self.level_list = [    "  d",
                               "  w",
                               "www"]
        
class LevelLoadingBoard(TiledBoard):
    def __init__(self, width, height):
        super().__init__(width, height)
        self.objects = {"w": Wall, "d": Door}
    
    def setup_room(room):
        for token in board.tokens:
            if token != player:
                token.remove()
        for i, row in enumerate(room):
            for j, column in enumerate(row):
                x = j
                y = i
                if room[i][j] in self.objects.keys():
                    self.objects[room[i][j]](x, y)
                
board = LevelLoadingBoard(width, height)
    