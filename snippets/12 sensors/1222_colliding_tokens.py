from miniworldmaker import *

board = Board(600, 200)
board.color = (100, 100, 160)
bat1 = Token()
bat2 = Token((600, 0))


@bat1.register
def act(self):
    self.x = self.x + 1


@bat2.register
def act(self):
    self.x = self.x - 1
    if self.detect_token():
        pass


board.run()
