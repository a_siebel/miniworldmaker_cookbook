from miniworldmaker import *
import random

board = Board()
board.add_background((255,0,0))
board.background.draw_color_on_image((100,100,100), (0,0), 100, 100)
for _ in range(150):
    board.background.draw_color_on_image((100,100,100), (random.randint(0,400), random.randint(0, 400)), 40, 40)


token = Circle((10,10))
token.direction = 90
@token.register
def act(self):
    if self.sense_color_at(self.direction, 1) == (255,0,0,255) or self.sense_color_at(self.direction, 1) == None:
        self.turn_right(random.randint(0,90))
    else:
        self.move()
board.run()