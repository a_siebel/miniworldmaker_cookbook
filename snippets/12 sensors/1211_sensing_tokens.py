from miniworldmaker import *
board = Board()
t1 = Token()
t1.color = (255,0,0)
t2 = Token(110,0)
t2.color = (0,0,255)
@t1.register
def act(self):
    self.x+=1
@t1.register
def on_detecting_token(self, other):
    print("detecting...")
    if other==t2:
        self.color = (255,255,0)
print(board.event_manager.registered_events)

board.run()
