from miniworldmaker import *
board = TiledBoard(6,6)
t1 = Token((3,3))
t2 = Token((3,4))
t3 = Token((3,3))
print(t3 in t1.detect_tokens())
assert(t3 in t1.detect_tokens())
assert(t2 not in t1.detect_tokens())

board.run()
