from miniworldmaker import *
board = Board()
board.add_background((255,0,0))

token = Token((10,10))
print(token.detect_color())
print(token.sense_color_at())
assert(token.detect_color((255,0,0)) is True)
assert(token.sense_color_at() == (255,0,0,255))
board.run()