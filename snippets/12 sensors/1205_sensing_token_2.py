from miniworldmaker import *
board = Board()
wall=Token((200,0))
wall.color = (255,255,255)
wall.size = (20, 400)

for i in range(7):
    token = Token((10,i*60))
    token.color = (255,255,255)
    token.range = i * 10
    token.number = i % 4
    @token.register
    def act(self):
        if self.number == 0:
            if not self.detect_token(wall):
                self.direction = "right"
                self.move()
        if self.number == 1:
            if not self.detect_token():
                self.direction = "right"
                self.move()
        if self.number == 2:
            if not self.detect_tokens():
                self.direction = "right"
                self.move()
        if self.number == 3:
            if not self.detect_tokens(wall):
                self.direction = "right"
                self.move()
            

board.run()