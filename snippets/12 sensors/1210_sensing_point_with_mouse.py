from miniworldmaker import *
board = Board()
token = Token()
token.color = (255,0,0, 100)
token.size = (16, 16)
@board.register
def on_mouse_motion(self, mouse_pos):
    token.center = mouse_pos
    self.background.draw((255,0,0), mouse_pos, 2, 2)

board.run()