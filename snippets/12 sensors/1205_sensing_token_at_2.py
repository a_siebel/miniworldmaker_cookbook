from miniworldmaker import *
board = Board()
wall=Token((200,0))
wall.size = (20, 400)

for i in range(7):
    token = Token((10,i*60))
    token.range = i * 10
    @token.register
    def act(self):
        if i % 4 == 0:
            if not self.detect_token(wall):
                self.direction = "right"
                self.move()
        elif i % 4 == 1:
            if not self.detect_token():
                self.direction = "right"
                self.move()
        elif i % 4 == 2:
            if not self.detect_tokens():
                self.direction = "right"
                self.move()
        elif i % 4 == 3:
            if not self.detect_tokens(wall):
                self.direction = "right"
                self.move()
            

board.run()