from miniworldmaker import *
board = Board()
wall=Rectangle((200,0))
wall.size = (20, 400)

for i in range(7):
    token = Circle((10,i*60 + 20))
    token.range = i * 10
    @token.register
    def act(self):
        if not self.detect_tokens_at(self.direction, self.range):
            self.direction = "right"
            self.move()

board.run()