from miniworldmaker import *
board = TiledBoard(6,6)
print(board.rows, board.columns)
for x in range(board.rows):
    for y in range(board.columns):
        color = (x * y) / (board.rows * board.columns) * 255
        board.background.draw((color, color, color), (x * board.tile_size,y * board.tile_size), 40,40)
token = Token((3,3))
print(token.detect_color((63,63,63)))
print(token.sense_color_at(distance = 1, direction = 90))
assert(token.detect_color((63,63,63)) is True)
assert(token.sense_color_at(distance = 1, direction = 90) == (85,85,85,255) )
board.run()
