from miniworldmaker import *
board = Board()
board.background.draw((255,0,0), (200,0), 20, 400)

for i in range(7):
    token = Token((10,i*60))
    token.range = i * 10
    @token.register
    def act(self):
        if not self.sense_color_at(self.direction, self.range) == (255,0,0,255):
            self.direction = "right"
            self.move()

board.run()