import miniworldmaker as mwm

board = mwm.Board()
token = mwm.Token((10,10))
token.color = (100, 200, 100)

token.direction = 90
token.is_blockable = True
@token.register
def act(self):
    self.direction = 90
    self.move()

wall = mwm.Token(100, 10)
wall.set_costume((100,100,100))
wall.is_blocking = True

wall2 = mwm.Token(50, 10)
wall2.set_costume((0,100,100))
wall2.is_blocking = False

board.run()