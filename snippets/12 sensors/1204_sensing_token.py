from miniworldmaker import *
board = Board()
t1 = Token((10,10))
t2 = Token((20,10))

t3 = Token((10,10))
print(t3.detect_tokens())
print(t1.detect_tokens())
print("t3 detect tokens:", len(t3.detect_tokens()))
assert(len(t3.detect_tokens())==2)
assert(len(t1.detect_tokens())==2)
assert(t1 in t3.detect_tokens())
assert(t2 in t3.detect_tokens())
board.run()