from miniworldmaker import *
import random

board = Board()

path = "music/saxophone_guy.mp3"
sound_path = "sounds/drum1.wav"
play_button = Token()
play_button.add_costume("images/playstop.png")
@play_button.register
def on_clicked_left(self, pos):
    global path
    if not self.board.music.is_playing():
        self.board.music.play(path)
    else:
        print("pause")
        self.board.music.pause()

volume_up_button = Token((60,0))
volume_up_button.add_costume("images/volume_up.png")
@volume_up_button.register
def on_clicked_left(self, pos):
    self.board.music.set_volume(self.board.music.get_volume() + 10)

volume_down_button = Token((120,0))
volume_down_button.add_costume("images/volume_down.png")
@volume_down_button.register
def on_clicked_left(self, pos):
    self.board.music.set_volume(self.board.music.get_volume() - 10)


music_a_button = Circle((40,80))
music_a_button.color = (255,0,0)
@music_a_button.register
def on_clicked_left(self, pos):
    global path
    path = "music/saxophone_guy.mp3"
    if not self.board.music.get_path == path:
        self.board.music.play(path)
    elif self.board.music.is_playing():
        self.board.music.play(path)
    else:
        self.board.music.pause()

music_b_button = Circle((80,80))
music_b_button.color = (0,255,0)
@music_b_button.register
def on_clicked_left(self, pos):
    global path
    path = "music/in_tune.mp3"
    if not self.board.music.get_path == path:
        self.board.music.play(path)
    elif self.board.music.is_playing():
        self.board.music.play(path)
    else:
        self.board.music.pause()
        
drum_buttons = list()
for i in range(4):
    drum_button = Circle(40 + i * 60, 120)
    drum_button.color = (0,0,i * 20)
    drum_button.path = "sounds/drum" + str(i + 1) + ".wav"
    @drum_button.register
    def on_clicked_left(self, pos):
        self.board.sound.play(self.path, volume = random.randint(0, 100))

                
board.run()