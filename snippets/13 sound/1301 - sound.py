import miniworldmaker

class MyBoard(miniworldmaker.TiledBoard):

    def on_setup(self):
        self.play_music("sounds/bensound-betterdays.mp3")

    def on_key_down(self, key):
        self.play_sound("sounds/olddoor.wav")


myboard = MyBoard()
myboard.run()
