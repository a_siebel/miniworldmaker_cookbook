from miniworldmaker import *
# music from https://opengameart.org/content/motivational-chips-day-2, cc0 license
board = Board()

board.play_music("sounds/demo.mp3")

@board.register
def act(self):
    print(self.music.is_playing())
    if self.frame == 200:
        board.stop_music()

board.run()