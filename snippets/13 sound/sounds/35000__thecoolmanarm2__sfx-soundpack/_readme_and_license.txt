Sound pack downloaded from Freesound
----------------------------------------

"SFX Soundpack"

This pack of sounds contains sounds by the following user:
 - TheCoolManARM2 ( https://freesound.org/people/TheCoolManARM2/ )

You can find this pack online at: https://freesound.org/people/TheCoolManARM2/packs/35000/


Licenses in this pack (see below for individual sound licenses)
---------------------------------------------------------------

Creative Commons 0: http://creativecommons.org/publicdomain/zero/1.0/


Sounds in this pack
-------------------

  * 631923__thecoolmanarm2__sfx-9.wav
    * url: https://freesound.org/s/631923/
    * license: Creative Commons 0
  * 631922__thecoolmanarm2__sfx-7.wav
    * url: https://freesound.org/s/631922/
    * license: Creative Commons 0
  * 631921__thecoolmanarm2__sfx-8.wav
    * url: https://freesound.org/s/631921/
    * license: Creative Commons 0
  * 631920__thecoolmanarm2__sfx-5.wav
    * url: https://freesound.org/s/631920/
    * license: Creative Commons 0
  * 631919__thecoolmanarm2__sfx-6.wav
    * url: https://freesound.org/s/631919/
    * license: Creative Commons 0
  * 631918__thecoolmanarm2__sfx-13.wav
    * url: https://freesound.org/s/631918/
    * license: Creative Commons 0
  * 631917__thecoolmanarm2__sfx-2.wav
    * url: https://freesound.org/s/631917/
    * license: Creative Commons 0
  * 631916__thecoolmanarm2__sfx-3.wav
    * url: https://freesound.org/s/631916/
    * license: Creative Commons 0
  * 631915__thecoolmanarm2__sfx-4.wav
    * url: https://freesound.org/s/631915/
    * license: Creative Commons 0
  * 631914__thecoolmanarm2__sfx-1.wav
    * url: https://freesound.org/s/631914/
    * license: Creative Commons 0
  * 631913__thecoolmanarm2__sfx-10.wav
    * url: https://freesound.org/s/631913/
    * license: Creative Commons 0
  * 631912__thecoolmanarm2__sfx-11.wav
    * url: https://freesound.org/s/631912/
    * license: Creative Commons 0
  * 631911__thecoolmanarm2__sfx-12.wav
    * url: https://freesound.org/s/631911/
    * license: Creative Commons 0


