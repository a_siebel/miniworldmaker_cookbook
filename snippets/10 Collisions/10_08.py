from miniworldmaker import *
board = Board(300, 300)

t1 = Token(260,260)
@t1.register
def act(self):
    self.direction = - 45
    self.move()

t2 = Token(0,0)
@t2.register
def act(self):
    self.direction = 135
    self.move()
    print(Direction.from_tokens(t1, t2))
          

board.run()