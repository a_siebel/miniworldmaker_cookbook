from miniworldmaker import *

board = TiledBoard(8,3)
token = Token((1,1))
token.add_costume("images/robo_green.png")
token.orientation = -90
token.direction = 90

token2 = Token((4,1))
token2.add_costume("images/robo_yellow.png")
token2.orientation = -90
token2.direction = -90

@token.register
def act(self):
    self.move()
    token = self.detect_token()
    if token:
        token.move_right()
board.run()