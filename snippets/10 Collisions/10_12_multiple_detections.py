from miniworldmaker import *

board = Board(100,100)
c1 = Circle((20,20), 20)
c2 = Circle((20,20), 20)
r = Rectangle((0,0),20, 20)
@c1.register
def on_detecting(self, token):
    if token == r:
        print("c1")
    pass

@c2.register
def on_detecting(self, token):
    if token == r:
        print("c2")
    pass
    

board.run()