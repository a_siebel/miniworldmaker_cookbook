from miniworldmaker import *

board = Board(100,100)
c1 = Circle((20,20), 20)
c2 = Circle((20,20), 20)
@c1.register
def on_detecting(self, token):
    if token == c2:
        print("c1 detected c2", self.board.frame)
    pass
@board.register
def act(self):
    if self.frame == 100:
        c2.remove()
board.run()