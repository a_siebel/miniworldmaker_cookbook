from miniworldmaker import *
import random

board = Board()

for i in range(50):
    Token((random.randint(0,board.width), random.randint(0,board.height)))

@timer(frames = 50)
def clean():
    board.clear()


board.run()