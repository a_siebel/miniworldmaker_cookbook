from miniworldmaker import Board, Token, Rectangle, ActionTimer

board = Board()

q = Token()

@q.register
def act(self):
    self.x = self.x + 1

class MyToken(Token):
    def act(self):
        if self.board.frame == 20:
            q.remove()
            self.remove()
        else:
            self.x = self.x + 1
            

t = MyToken()
board.run()