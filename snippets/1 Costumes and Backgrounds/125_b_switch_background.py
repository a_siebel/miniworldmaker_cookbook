from miniworldmaker import *

board = Board()
token = Token()

board.add_background("images/1.png")
board.add_background((255, 0, 0, 255))
board.add_background("images/2.png")

@timer(frames = 40)
def switch():
    board.switch_background(0)

@timer(frames = 80)
def switch():
    board.switch_background(1)
    
@timer(frames = 160)
def switch():
    board.switch_background(2)
    
board.run()