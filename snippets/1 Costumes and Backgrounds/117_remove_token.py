from miniworldmaker import Board, Token, Rectangle, ActionTimer

board = Board()
player = Token()
rectangle = Rectangle(80, 80, 80, 80)
ActionTimer(20, player.remove, None)
ActionTimer(40, rectangle.remove, None)
board.run()