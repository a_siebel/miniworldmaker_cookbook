from miniworldmaker import *
board = HexBoard(8, 8)

@board.register
def on_setup(self):
    self.clear_tiles()
    center = HexTile((4, 4))
    for x in range(self.columns):
        for y in range(self.rows):
            if center.position.distance((x, y)) < 2:
                tile = self.add_tile_to_board((x, y))
                tile.create_token()
                if x == 3 and y == 3:
                    corner = self.add_corner_to_board((x,y), "n")
                    c =corner.create_token()
                    c.color=(255,0,0)
                    c.size = (0.2, 0.2)
                    edge = self.add_edge_to_board((x,y), "w")
                    e = edge.create_token()
                    e.color = (0,0,255)
                    e.size = (0.2, 0.8)
                    e.direction = edge.angle
                    e.layer = 1

board.run()
