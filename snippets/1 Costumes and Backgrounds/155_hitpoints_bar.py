from miniworldmaker import *

board = Board()

status_bar = Rectangle((10, 10), 100, 20)
status_bar.color = (0, 255, 0)
status_bar.border_color = (255, 255, 255)

status_bar.layer = 2

status_border = Rectangle((10,10), 100, 20)
status_border.color = (0, 0, 0)
status_border.border_color = (255, 255, 255)

status_border.layer = 1

@loop(frames=20)
def shrink():
    if status_bar.width > 0:
        status_bar.width -= 10


board.run()