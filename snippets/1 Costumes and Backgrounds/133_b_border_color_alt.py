from miniworldmaker import *

board = Board(210,80)
board.default_border_color = (0,0, 255)
board.default_border = 1

t = Token((10,10)) # default-border and color from bord
t.add_costume("images/player.png")

t2 = Token ((60, 10)) # overwrites default border values
t2.add_costume("images/player.png")
t2.border_color = (0,255, 0)
t2.border = 5 

t3 = Token ((110, 10)) # removes border
t3.add_costume("images/player.png")
t3.border = None 



board.run()

