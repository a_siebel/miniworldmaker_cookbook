from miniworldmaker import *
board = Board()
token = Token()
token.remove_costume()
assert token.costume_count == 0
token.add_costume((255,0,0))
assert token.costume_count == 1
token.add_costume((0,255,0))
print(token.costumes)
assert token.costume_count == 2
token.remove_costume()
print(token.costumes)
assert token.costume_count == 1
token.remove_costume()
assert token.costume_count == 0
board.run()