from miniworldmaker import *
board = Board()
token = Token()
token.remove_costume()
assert token.costume_count == 0
token.add_costume((255,0,0,0))
assert token.costume_count == 1
board.run()