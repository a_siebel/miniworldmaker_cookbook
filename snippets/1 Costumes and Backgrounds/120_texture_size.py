from miniworldmaker import *

board = Board()
background = board.add_background("images/stone.png")
background.is_textured = True
background.texture_size = (15,15)
board.run()
