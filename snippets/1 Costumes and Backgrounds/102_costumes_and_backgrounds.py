from miniworldmaker import *

board = Board(200,400)
#board.add_background("images/stone.jpg")
board.tile_size = 40
board.add_background("images/grass.jpg")
token = Token()
token.position = (4,4)
token.add_costume("images/player.png")

# Token 1: Purple in topleft corner
token1 = Token(position=(0, 0))
token1.size = (40, 40) # should be in topleft corner
assert token1.position,(0,0)

token2 = Token(position=(40, 40))
assert token2.position,Position(40, 40)
token2.size = (40, 40)
assert token2.position, Position(40, 40)
assert token2.size,(40,40)

# Token 3: Below token1, created with Image "1"
token3 = Token(position=(40, 80))
token3.add_costume("images/1.png")
token3.size = (40, 40)
assert token3.position, Position(40, 80)

# Token 4: Below token1, created with Image "2" in `on_setup`-Method
class MyToken(Token):
    def on_setup(self):
        self.add_costume("images/2.png")

token4 = MyToken(position = (40,130))
assert token4.position, Position(40, 130)

# Token5: Created with image "3" without file ending
token5 = Token(position=(60, 200))
token5.add_costume("images/3")
assert token5.position == Position(60, 200)
print(token5.costume.image.get_width())
#assert token5.costume.image.get_width() == 40
#bassert token5.costume.image.get_height() == 40

# Token6: Created with images "1" and "2", siwtches from 
class SwitchBackground(Token):
    def on_setup(self):
        self.add_costume("images/1")
        self.add_costume("images/2")
              
token6 = SwitchBackground(position = (60,250))
# Token 7: Like 6, but switches to costume 1 (remember, counting from 0)
token7 = SwitchBackground(position = (67,307))
print(token7.costume)
print(token7.costume_manager.appearance)
token7.switch_costume(1)

# Token 7 throws error because switching to costume 2 is not allowd
#with test.assertRaises(CostumeOutOfBoundsError):
#    token7.switch_costume(2)

# Token 8: Purple in topleft corner (with center)
token8 = Token()
token8.size = (40,40)
token8.center=(200,0)
token8.costume.set_dirty("all", 2)
print(board.camera.is_token_in_viewport(token8))
print(token8.rect)
print(token8.get_global_rect())
print(board.camera.get_rect())
print(token.get_global_rect().colliderect(board.camera.get_rect()))

board.run()
