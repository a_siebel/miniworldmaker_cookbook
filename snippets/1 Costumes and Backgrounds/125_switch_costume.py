from miniworldmaker import *

board = Board(120,60)
t1 = Token()
costume =t1.add_costume("images/1.png")
t1.add_costume("images/2.png")
t1.switch_costume(1)

t2 = Token((40,0))
t2.add_costume((100,0,0))
t2.add_costume((0,100,0))

@timer(frames = 40)
def switch():
    print("switch t1")
    t1.switch_costume(0)

@timer(frames = 80)
def switch():
    print("switch t2")
    t2.switch_costume(0)
    print("switch t3")
    t3.switch_costume(0)

    
@timer(frames = 120)
def switch():
    print("switch t2")
    t2.switch_costume(1)
    print("switch t3")
    t3.switch_costume(1)
    

t3 = Token((80,0))
t3.add_costume("images/1.png")
t3.add_costume("images/2.png")
board.run()
