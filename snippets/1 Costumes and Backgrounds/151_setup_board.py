from miniworldmaker import *
board = HexBoard(8, 8)

@board.register
def on_setup(self):
    center = HexTile((4, 4))
    for x in range(self.columns):
        for y in range(self.rows):
            if center.position.distance((x, y)) < 2:
                tile = self.get_tile((x, y))
                tile.create_token()


board.run()
