from miniworldmaker import *

board = Board()
token = Token()
costume = token.add_costume("images/1.png")
costume.add_image("images/2.png")
assert(len(token.costumes) == 1)
assert(len(token.costume.images) == 2)
board.run()
