from miniworldmaker import *

board = Board(800,400)

def create_player(x, y):
  t = Token()
  t.position = (x, y)
  t.add_costume("images/player.png")
  t.border = 1
  return t

t = create_player(0,180)
t.size=(80,80)

t = create_player(80,180)
t.costume.is_upscaled = True
t.size=(80,80)

t = create_player(160,180)
t.costume.is_scaled = True
t.size=(80,80)

board.run() 
