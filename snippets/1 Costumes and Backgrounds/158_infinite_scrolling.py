from miniworldmaker import *

# basiert auf https://github.com/kantel/pygamezero/tree/master/scrolllingbg

WIDTH, HEIGHT = 800, 400
board = Board(WIDTH, HEIGHT)
left, bottom = WIDTH/2, HEIGHT/2

# background from: https://opengameart.org/content/several-scrolling-backgrounds-and-layerable-runners (cc0)
BACKGROUND = "desertback"

back0 = Token()
back0.add_costume(BACKGROUND)
back0.size = WIDTH, HEIGHT
back1 = Token(WIDTH, 0)
back1.size = WIDTH, HEIGHT
back1.add_costume(BACKGROUND)
backs = [back0, back1]

# sprites from kenney assets, CC0

walker = Token((100, HEIGHT - 100))
walker.size = 100, 60
walker.add_costumes(["walk1", "walk2"])
walker.speed = 1
walker.count = 0

@board.register
def act(self):
    for back in backs:
        back.x -= 1
        if back.x <= - WIDTH:
            back.x = WIDTH
    walker.count += walker.speed
    if walker.count > 11:
        costume = walker.next_costume()
        walker.count = 0

@board.register
def on_key_down(self, keys):
    if "q" in keys:
        board.quit
        
board.run()
