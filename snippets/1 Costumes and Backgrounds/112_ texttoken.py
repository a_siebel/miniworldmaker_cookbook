from miniworldmaker import *
board = Board((200,200))
board.default_fill_color = (0,255,255,255)

token = Text((0,0), "Hello World!")
token.font_size = 32
print("TOKEN TEXT", token.costume, "TEXT", token.text, token.costumes)

token2 = Text((0,60))
token2.set_text("Hello!")
token2.font_size = 32

token3 = Number((0,150))
token3.font_size=64

board.run()