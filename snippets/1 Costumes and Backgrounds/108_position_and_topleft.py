from miniworldmaker import *

board = Board(400, 400)
board.add_background((100, 0, 0, 255))

a = Token()
a.position = (0,0)
print(a.topleft)
print(a.position)
b = Token()
b.topleft = (100,100)
print(b.topleft)
print(b.position)
c = Token()
c.position = (200,200)
print(c.topleft)
print(c.position)
d = Token()
d.center = (250,250)
print(d.topleft)
print(d.position)
board.run()