from miniworldmaker import *

board = Board(210,80)
board.default_border_color = (0,0, 255)
board.default_border = 1

t = Token((10,10))

t2 = Token ((60, 10))
t2.border_color = (0,255, 0)
t2.border = 5 # overwrites default border

t3 = Token ((110, 10))
t3.border = None # removes border

t4 = Token ((160, 10))
t4.add_costume("images/player.png") # border for sprite

board.run()

