import miniworldmaker

class MyBoard(miniworldmaker.TiledBoard):
    def on_setup(self):
        self.columns = 5
        self.rows = 5
        self.tile_size = 40
        self.add_background("images/soccer_green.jpg")
        self.default_border_color = (255,100,100,255)
        self.default_border = 5
        
        token = miniworldmaker.Token()
        token.position = (0, 0)
        token.add_costume((100, 100, 100))
        
        token2 = miniworldmaker.Token()
        token2.fill = True
        token2.fill_color = (0,100,0)
        token2.border = 10
        token2.costume.stroke_color = (0,200,0)
        token2.position = (1, 0)
        
        token = miniworldmaker.Token()
        token.costume.add_image((0,200,200))
        token.position = (2, 0)
    
board = MyBoard(8, 6)
board.run()
