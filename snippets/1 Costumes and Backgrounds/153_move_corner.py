from miniworldmaker import *
global actor
board = HexBoard(10, 10)

for position, tile in board.tiles.items():
    tile.create_token()

@board.register
def on_setup(self):
    corner = self.get_corner((1,1), "nw")
    token = corner.create_token()
    token.color = (255,0,0)
    token.direction = "right"
    @token.register
    def act(self):
        token.move()
        if not token.sensing_on_board():
            token.move_back()
        print(token.position)
board.run()
