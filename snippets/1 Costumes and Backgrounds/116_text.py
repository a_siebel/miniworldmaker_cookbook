from miniworldmaker import *

board = PixelBoard()
board.size = (600,300)

t1 = Text((50,100), "Hello World 1")
t1.border = 1
t1.font_size = 50
t1.direction = 5

t2 = Text((0,0), "Hello World 2")
t2.border = 1
t2.font_size = 20
t2.position = (50,50)

t3 = Text((0,0))
t3.text = "Hello World 3"
t3.border = 1
t3.font_size = 20
t3.position = (50,200)

t4 = Text((0,0))
t4.text = "Hello World 4"
t4.border = 1
t4.color = (255,100,100,100)
t4.font_size = 20
t4.position = (20,280)

board.run()
