from miniworldmaker import *

board = TiledBoard()
# Black board
board.add_background((200, 200, 200, 255))
board.columns = 5
board.rows = 5
board.tile_size = 40

# Token1 at position (2,1) with player costume
token1 = Token(position=(2, 1))
token1.add_costume("images/player.png")
print(token1.costumes, token1.costume.images)
# Token2 at position (3,1) with purple backgrund
token2 = Token(position = (3, 1) )
token2.add_costume((100,0,100,100))
print(token2.get_local_rect())
try:
    token2.size = (1,1)
except Exception as e:
    print(e)
token2.positin = (40,40)
print(token2.x, token2.y)

token2 = Token(position = (3, 2) )
token2.add_costume((100,0,100,100))

token3 = Token(position=(3, 2))
token3.add_costume("images/player.png")

print(board.camera.get_rect())
print(token3.get_rect())
board.run()