from miniworldmaker import *

board = TiledBoard(4,4)
board.tile_margin = 10
background = board.add_background("images/stone.png")
background.is_textured = True
token = Token()
@token.register
def on_key_down(self, key):
    self.move_right()
background.grid = True
board.run()
