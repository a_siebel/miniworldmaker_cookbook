from miniworldmaker import *

board = TiledBoard(2,2)
token = Token()

@timer(frames = 24)
def hide():
    token.hide()

@loop(frames = 48)
def show():
    token.show()
    
board.run()
