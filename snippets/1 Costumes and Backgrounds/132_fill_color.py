from miniworldmaker import *

board = Board(200,80)
board.default_fill_color = (0,0, 255)

t = Token()

t2 = Token((40,0))
t2.is_filled = (0, 255, 0)

t3 = Token((80, 0))
t3.fill_color = (255, 0, 0)

t4 = Token((120, 0))
t4.add_costume((0,0,0))
t4.fill_color = (255, 255, 0)

t5 = Token((160, 0))
t5.add_costume("images/player.png")
t5.fill_color = (255, 255, 0, 100) # image is overwritten
assert (t5.is_filled == (255, 255, 0, 100))

t6 = Circle((0, 40), 20)
t6.position = t6.center
t6.fill_color = (255, 255, 255)

t7 = Ellipse((40, 40), 40, 40)
t7.fill_color = (255, 0, 255) 



board.run()

