from miniworldmaker import *

board = TiledBoard()

t1 = Token((4,4))
t1.add_costume("images/player.png")
t1.move()

t2 = Token((4,5))
t2.add_costume("images/player.png")
t2.orientation = - 90
t2.move()

@t1.register
def act(self):
    self.move()

@t2.register
def act(self):
    self.move()
    
board.run()