from miniworldmaker import *

class MyBoard(TiledBoard):
    def on_setup(self):
        self.columns = 5
        self.rows = 5
        self.tile_size = 40
        self.add_background("images/soccer_green.jpg")
        self.border_color = (0,0,0,255)
        token = Token()
        token.position = (3,4)
        token.border = 2
        token.add_costume("images/player.png")
        
        
board = MyBoard(8, 6)
board.run()
