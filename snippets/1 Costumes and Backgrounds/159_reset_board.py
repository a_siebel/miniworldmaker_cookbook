from miniworldmaker import *

board = Board(400,600)

@board.register
def on_setup(self):
    print("on setup")
    self.add_background((0,255,0,255))
    self.token = Token((10,10))
    print(self.backgrounds)

@board.register
def act(self):
    if self.frame == 20:
        self.token.x += 100
    if self.frame == 40:
        self.reset()
        

board.run()