from miniworldmaker import *

board = Board((100,60))
token = Token((10,10))
board.speed = 30
costume1 = token.add_costume((255,255,0))
costume2 = token.add_costume((255,0,255))
@token.register
def act(self):
    if self.costume == costume1:
        self.switch_costume(costume2)
    else:
        self.switch_costume(costume1)

board.run()