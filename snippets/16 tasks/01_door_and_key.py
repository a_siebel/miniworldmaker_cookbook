from miniworldmaker import *

class Door(Token):
    def __init__(self, position):
        super().__init__(position)
        self.costume_opened = self.add_costume("images/door_open.png")
        self.costume_closed = self.add_costume("images/door_closed.png")
        self.switch_costume(self.costume_closed)
    
    def open_door(self, player):
        if player.has_key:
            self.switch_costume(self.costume_opened)
        
class Player(Token):
    def __init__(self, position):
        super().__init__(position)
        self.add_costume("images/player.png")
        self.costume.is_rotatable = False
        self.has_key = False
        
    def act(self):
        detected_token = self.detect_token_in_front()
        if not detected_token:
            self.move()
        elif isinstance(detected_token, Wall):
            self.turn_right(90)
        elif isinstance(detected_token, Key):
            self.move()
            detected_token.take(self)
        elif isinstance(detected_token, Door):
            self.move()
            detected_token.open_door(self)           
        
class Key(Token):
    def __init__(self, position):
        super().__init__(position)
        self.add_costume("images/key.png")
        
    def take(self, player):
        self.remove()
        player.has_key = True
        
        
class Wall(Token):
    def __init__(self, position):
        super().__init__(position)
        self.add_costume("images/wall.png")
            
board = TiledBoard()
board.add_background((0,0,0))


door = Door((4,4))
key = Key((5,4))
player = Player((1,1))
player.direction = "right"
for i in range(0, 6):
    Wall(0,i)
    Wall(6,i)
for i in range(1, 6):
    Wall(i,0)
    Wall(i,5)

board.run()

