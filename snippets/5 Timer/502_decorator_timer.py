from miniworldmaker import *

board = TiledBoard()
board.columns=20
board.rows=8
board.tile_size=40
board.add_background("images/soccer_green.jpg")
board.add_background("images/space.jpg")
board.speed = 30

player = Token(position=(3, 4))
player.add_costume("images/char_blue.png")
player.costume.orientation = - 90

@timer(frames = 24)
def moving():
    player.move()

@loop(frames = 48)
def moving():
    player.turn_left()
    player.move(2)

board.run()
