from miniworldmaker import *

board = HexBoard(24, 2)

board.tile_size = 40
for x in range(board.columns):
    for y in range(board.rows):
        t = Token((x,y))
        color = (x + y) / (board.columns + board.rows) * 255
        t.fill_color = (color, color, color)
        
board.run()