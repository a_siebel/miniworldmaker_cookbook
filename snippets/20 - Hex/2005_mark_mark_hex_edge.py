from miniworldmaker import *
# not working yet
board = HexBoard(12, 12)

for x in range(board.columns):
    for y in range(board.rows):
        t = Token((x,y))
        t.static = True
        t.add_costume(f"images/grass_08.png")


last_edge = None

@board.register
def on_mouse_motion(self, mouse_pos):
    global last_edge
    if last_edge:
        for token in last_edge.get_tokens():
            if hasattr(token, "marker") and token.marker:
                token.remove()
    
    edge = HexEdge.from_pixel(mouse_pos)
    h = edge.create_token()
    h.border = 1
    h.fill_color = (255,255,255)
    h.is_filled = True
    h.size = 0.2, 1
    h.direction = edge.angle
    h.marker = True
    last_edge = edge

board.run()