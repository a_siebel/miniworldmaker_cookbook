from miniworldmaker import *
# not working yet
board = HexBoard(6, 6)
last_tile = None
board.tile_size = 40

@board.register
def on_mouse_motion(self, mouse_pos):
    global last_tile
    if last_tile:
        for token in last_tile.get_tokens():
            if hasattr(token, "marker") and token.marker:
                token.remove()
    
    tile = HexTile.from_pixel(mouse_pos)
    h = tile.create_token()
    h.border = 1
    h.fill_color = (255,255,255)
    h.is_filled = True
    h.marker = True
    last_tile = tile

@board.register
def on_mouse_left(self, mouse_pos):
    tile = mouse_pos
    h = tile.create_token()
    h.border = 1
    h.fill_color = (255,255,0)
    h.is_filled = True
    h.marker = False

board.run()