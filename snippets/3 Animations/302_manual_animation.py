from miniworldmaker import *

board = Board(280, 100)
board.add_background("images/water.png")
board.speed = 1

robo = Token(position=(0, 0))
robo.costume.add_images(["images/1.png", "images/2.png","images/3.png","images/4.png"])
ActionTimer(80,robo.costume.set_image,1)
ActionTimer(120,robo.costume.set_image,2)
ActionTimer(150,robo.costume.set_image,3)
robo.costume.animation_speed = 200
board.run()