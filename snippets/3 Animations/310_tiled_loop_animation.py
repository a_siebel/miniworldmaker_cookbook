from miniworldmaker import *

board = TiledBoard(columns=5, rows=2)
board.add_background("images/water.png")
print(board.speed, board.fps)
robo = Token(position=(0, 0))
robo.costume.add_images(["images/1.png", "images/2.png","images/3.png","images/4.png"])
robo.costume.loop = True
robo.costume.animate()
robo.costume.orientation = - 90
robo.costume.animation_speed = board.speed
robo.direction = "right"
@robo.register
def act(self):
    if self.detect_on_board():
        self.move()
@robo.register
def on_not_detecting_board(self):
    self.flip_x()
    self.move()

board.run()

