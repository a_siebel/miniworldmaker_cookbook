from miniworldmaker import *

board = PixelBoard(280, 100)
board.add_background("images/water.png")
board.speed = 1
print(board.speed, board.fps)
robo = Token(position=(0, 0))
robo.costume.add_images(["images/1.png", "images/2.png","images/3.png","images/4.png"])
print(robo.costume)
robo.size = (99, 99)
robo.costume.loop = True
robo.costume.animate()
robo.costume.orientation = - 90
robo.costume.animation_speed = 20
robo.direction = "right"
@robo.register
def act(self):
    if self.detect_board():
        self.move()
@robo.register
def on_not_detecting_board(self):
    self.flip_x()
    self.move()

board.run()

