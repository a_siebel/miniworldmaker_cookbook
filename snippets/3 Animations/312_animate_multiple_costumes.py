from miniworldmaker import *

board = PixelBoard(200, 100)
board.add_background("images/water.png")
board.speed = 1
robo = Token(position=(10, 0))
c1 = robo.add_costume(["images/1.png", "images/2.png"])
c2 = robo.add_costume(["images/3.png","images/4.png"])
robo.switch_costume(c1)
robo.size = (99, 99)
c1.animate()
c1.loop = True
robo.costumes.orientation = - 90
robo.costumes.animation_speed = 20
robo.direction = "right"
print(robo.costumes)
for costume in robo.costumes:
    print(costume)
@robo.register
def act(self):
    if self.board.frame % 100 == 0:
        self.switch_costume(c2)
        c2.animate()
        
    if self.board.frame % 200 == 0:
        self.switch_costume(c1)
        c1.animate()

board.run()


