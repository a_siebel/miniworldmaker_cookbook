from miniworldmaker import *

board = Board(columns=280, rows=100)
robo = Token(position=(0, 0))
robo.costume.add_images(["images/1.png", "images/2.png","images/3.png","images/4.png"])
robo.size = (99, 99)
robo.animate_loop()
@timer(frames = 100)
def stop():
    robo.stop_animation()
board.run()

