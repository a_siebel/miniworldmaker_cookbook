from miniworldmaker import *

board = PixelBoard(columns=280, rows=100)
board.add_background("images/water.png")
board.speed = 1

robo = Token(position=(0, 0))
robo.costume.add_images(["images/1.png", "images/2.png","images/3.png","images/4.png"])
@loop(frames = 5)
def next_image():
    robo.costume.next_image()
    
robo.costume.animation_speed = 200
board.run()