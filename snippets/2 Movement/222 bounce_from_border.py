from miniworldmaker import *
import random

board = Board(150, 150)
token = Token((50,50))
token.add_costume("images/ball.png")
token.direction = 10

@token.register
def act(self):
    self.move()
    borders = self.detect_borders()
    if borders:
        self.bounce_from_border(borders)
        
board.run()