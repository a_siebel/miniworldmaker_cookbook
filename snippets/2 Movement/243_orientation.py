from miniworldmaker import *

board = Board()

class Arrow(Token):

    def on_setup(self):
        self.size = (30, 30)
        self.add_costume("images/tank_arrowFull.png")
        self.orientation = -90
        self.direction = 90
        
    def on_key_down_w(self):
        self.turn_left(2)
        
arrow = Arrow()

board.run()