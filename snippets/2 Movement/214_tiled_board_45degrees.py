from miniworldmaker import *

board = TiledBoard()
t = Token()
t.position = (3, 3)
    
x = 0
@board.register
def act(self):
    global x
    x = x + 1
    if x == 1:
        t.direction = 135
        t.move()
    if x == 2:
        t.move_in_direction(45)

board.run()