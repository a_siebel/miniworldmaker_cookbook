from miniworldmaker import *
board = TiledBoard()
t1 = Token((0,0))
t2 = Token((3,4))
t2.dragged = False

@t2.register
def on_mouse_left(self, mouse_pos):
    print(self.detect_point(mouse_pos))
    if self.detect_point(mouse_pos):
        self.dragged = True
        print("start drag")
        
@t2.register
def on_mouse_left_released(self, mouse_pos):
    pos = board.get_board_position_from_pixel(mouse_pos)
    print("released")
    if self.dragged:
        self.position = pos
        print("end drag")
    self.dragged = False
        
board.run()