from miniworldmaker import *

board=TiledBoard()
board.columns = 4
board.rows = 1
board.add_background("images/water.png")
fish = Token()
fish.border = 1
fish.add_costume("images/fish.png")
fish.direction = "right"
fish.orientation = -90
@fish.register
def act(self):
    print("act", self.position, self.direction)
    self.move()

@fish.register
def on_not_detecting_board(self):
    self.move_back()
    self.flip_x()
        
board.run()
