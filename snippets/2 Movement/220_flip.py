from miniworldmaker import *

board = Board()
token = Token()
token.add_costume("images/alien1.png")
token.height= 400
token.width = 100
token.is_rotatable = False
@token.register
def act(self):
    if self.board.frame % 100 == 0:
        print("flip")
        if self.costume.is_flipped:
            self.costume.is_flipped = False
        else:
            self.costume.is_flipped = True
board.run() 