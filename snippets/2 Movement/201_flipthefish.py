from miniworldmaker import *

board=TiledBoard(4, 1)
board.add_background("images/water.png")


class Fish(Token):

    def on_setup(self):
        self.add_costume("images/fish.png")
        self.costume.orientation = - 90
        self.direction = "right"

    def act(self):
        print("act", self.position, self.direction)
        self.move()

    def on_not_detecting_board(self):
        #print("move back")
        self.move_back()
        self.flip_x()
        

fish = Fish()
board.run()
