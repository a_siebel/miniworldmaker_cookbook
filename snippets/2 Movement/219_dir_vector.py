from miniworldmaker import *

board = Board()

player = Rectangle((200,200),40, 40)
player.speed = 1
player.direction = 80

@player.register
def act(self):
    v1 = Vector.from_token_direction(self)
    v1.rotate(-1)
    self.direction = v1
    
board.run()
