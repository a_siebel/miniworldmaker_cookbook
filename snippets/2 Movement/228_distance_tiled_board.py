from miniworldmaker import *

board = TiledBoard(8,8)
tile1 = board.get_tile((0,0))
tile2 = board.get_tile((6,6))
t = Token(5,6)
tile3 = Tile.from_token(t)
tile4 = board.get_tile((7,7))
t2 = Token(7,7)

print(tile1)
print(tile2)
print(tile1.distance_to(tile2))
print(tile3.distance_to(tile2))
print(tile1.distance_to(tile4))
assert 8 < tile1.distance_to(tile2) < 8.5
assert 0.9 < tile3.distance_to(tile2) < 1.1
assert 9.5 < tile1.distance_to(tile4) < 10
assert tile3.distance_to(tile4) == t.get_distance_to((7,7))
print(t.get_distance_to(t2), tile3.distance_to(tile4))
assert t.get_distance_to(t2) == tile3.distance_to(tile4)
board.run()