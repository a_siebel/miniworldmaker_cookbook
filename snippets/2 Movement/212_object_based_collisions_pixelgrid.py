from miniworldmaker import *
import pygame

board = Board()
board.columns=300
board.rows=200
board.add_background("images/soccer_green.jpg")
board.add_background("images/space.jpg")
board.speed = 30

player1 = Token(position=(30, 4))
player1.size = (40, 40)
player1.add_costume("images/char_blue.png")
player1.costume.orientation = - 90

player2 = Token(position=(3, 4))
player2.size = (40, 40)
player2.add_costume("images/char_blue.png")
player2.costume.orientation = - 90

player3 = Token(position=(90, 4))
player3.size = (40, 40)
player3.add_costume("images/char_blue.png")
player3.costume.orientation = - 90

@player1.register
def act(self):
        assert(player1.rect == pygame.Rect(30, 4, 40, 40))


@player2.register
def act(self):
    assert(player2.rect == pygame.Rect(3, 4, 40, 40))

@player1.register
def on_detecting_token(self, token):
    global player2
    assert(token==player2)
board.run()