from miniworldmaker import *


class MyBoard(TiledBoard):
    def on_setup(self):
        robot1 = Robot(position=(0, 0))
        robot1.add_costume("images/robo_green.png")
        robot1.costume.orientation = - 90
        robot1.direction = "right"
        robot2 = Robot(position=(4, 0))
        robot2.add_costume("images/robo_yellow.png")
        robot2.costume.orientation = - 90
        robot2.direction = "left"
        self.add_background("images/water.png")


class Explosion(Token):
    def on_setup(self):
        self.add_costume("images/explosion.png")
        @timer(frames=5)
        def remove():
            self.remove()
            

class Robot(Token):

    def act(self):
        self.move()
        other = self.detect_token(token_filter=Robot)
        if other:
            explosion = Explosion(position=self.position)
            print("exploded")
            self.remove()
            print("self.removed")
            other.remove()
            print("other.removed")


board = MyBoard(5, 1)
board.run()
