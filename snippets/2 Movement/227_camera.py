from miniworldmaker import *

board = Board()

token = Token(0,20)
token2 = Token(800,20)

@board.register
def act(self):
    token.x += 1
    if token.x >= 200:
        self.camera_x += 1
    token2.x -= 1
@token.register
def on_detecting_token(self, other):
    print("found you")
    
@token2.register
def on_not_detecting_board(self):
    print("not on board", self.position)
@token2.register
def on_detecting_board(self):
    print("on board")
    
board.run()