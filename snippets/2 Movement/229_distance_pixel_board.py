from miniworldmaker import *

board = Board(8,8)
t = Token(0,0)
t2 = Token(201, 201)
pos = t2.center
print(t.get_distance_to(t2))
print(t.get_distance_to(pos))
assert t.get_distance_to(t2) == t.get_distance_to(pos)
assert 284 < t.get_distance_to(pos) < 285
board.run()