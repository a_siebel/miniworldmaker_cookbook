from miniworldmaker import *

board = Board()

player = Rectangle((200,200),40, 40)
player.speed = 1
player.direction = 80

player2 = Rectangle((300,300),40, 40)
player2.speed = 1
player2.direction = 80

player3 = Rectangle((100,100),40, 40)
player3.speed = 1
player3.direction = 80

player4 = Rectangle((100,300),40, 40)
player4.speed = 1
player4.direction = 80


v4 = Vector.from_token_direction(player4)

@player.register
def act(self):
    v1 = Vector.from_token_direction(self)
    v1.rotate(-1)
    self.direction = v1
    
@player2.register
def act(self):
    self.direction = self.direction + 1
    
    
@player3.register
def act(self):
    v3 = Vector.from_token_direction(self)
    v3.rotate(1)
    self.direction = v3
   
    
@player4.register
def act(self):
    v4.rotate(1)
    self.direction = v4
board.run()