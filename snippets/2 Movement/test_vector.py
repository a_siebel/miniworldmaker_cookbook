from miniworldmaker import *


board = Board()
token = Token()
print ("test to_direction")
token.direction = 0
print("0", Vector.from_token_direction(token))
token.direction = 45
print("45", Vector.from_token_direction(token))
token.direction = 90
print("90", Vector.from_token_direction(token))
token.direction = 135
print("135",Vector.from_token_direction(token))
token.direction = 180
print("180", Vector.from_token_direction(token))
token.direction = 225
print("225", Vector.from_token_direction(token))

token.direction = -45
print("-45", Vector.from_token_direction(token))
token.direction = -90
print("-90", Vector.from_token_direction(token))
token.direction = -135
print("-135", Vector.from_token_direction(token))
print("---")
vector = Vector(0,1)
print(vector, vector.to_direction())
vector.rotate(-90)
print(vector, vector.to_direction())
print(Vector.from_token_direction(token).to_direction())
vector.rotate(20)
token.direction = vector
print(Vector.from_token_direction(token).to_direction())


print ("---- test operations --")

v = Vector(3, 1)
u = Vector(2, 5)
a = 5
print(u + v)
print(u.add(v))
print(u - v)
print(u.sub(v))
print(u.multiply(a))
print(u * a)
print(u * v)
print(u.multiply(v))
print(u.neg())
print(- u)

w = Vector(4, 3)
print(w.length())
print(w.normalize())
board.run()