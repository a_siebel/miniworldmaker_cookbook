from miniworldmaker import *

board = TiledBoard()
board.columns = 8
board.rows = 8
board.speed = 30
player = Token()
player.add_costume("images/player_1.png")
player.position = (4, 4)

@player.register
def act(self):
    self.move_towards((7, 7))
board.run()

