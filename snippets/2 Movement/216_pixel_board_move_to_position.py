from miniworldmaker import *

board = PixelBoard()
board.columns = 400
board.rows = 400
player = Token()
player.add_costume("images/player_1.png")
player.position = (200,200)

@player.register
def act(self):
    self.move_in_direction((360, 360))

board.run()


