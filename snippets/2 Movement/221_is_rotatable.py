from miniworldmaker import *
board = Board()

t1 = Token((100,100))
t1.add_costume("images/alien1.png")

t2 = Token((200,200))
t2.add_costume("images/alien1.png")
t2.is_rotatable = False

@t1.register
def act(self):
    self.move()
    self.direction += 1

@t2.register
def act(self):
    self.move()
    self.direction += 1

board.run()