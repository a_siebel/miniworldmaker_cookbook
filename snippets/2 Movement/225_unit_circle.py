from miniworldmaker import *

board = Board((800, 600))

a = Circle()
a.direction  = 45
assert(a.direction == 45)
assert(a.direction_at_unit_circle == 45)
a.direction  = 0
assert(a.direction == 0)
assert(a.direction_at_unit_circle == 90)
a.direction  = -90
assert(a.direction == -90)
print(a.direction_at_unit_circle)
assert(a.direction_at_unit_circle == -180)
a.direction = 180
print(a.direction)
assert(abs(a.direction) == 180)
assert(a.direction_at_unit_circle == -90)

a.direction_at_unit_circle = -90
assert(abs(a.direction) == 180)
print(a.direction_at_unit_circle)
assert(a.direction_at_unit_circle == -90)

a.direction_at_unit_circle = -180
print(a.direction)
assert(a.direction == -90)
print(a.direction_at_unit_circle)
assert(a.direction_at_unit_circle == -180)
board.run()


