from miniworldmaker import *

board = PhysicsBoard((400, 200))
anchors = []
circles = []

def add_line(obj1, obj2):
    print(obj1.center, obj2.center)
    l = Line(obj1.center, obj2.center)
    l.physics.simulation = None
    l.border = 1
    # l.fill = True
    l.color = (255,0,0,100)
    @l.register
    def act(self):
        self.start_position = obj1.center
        self.end_position = obj2.center
    
for i in range(5):
    anchor = Token()
    anchor.size = (20,20)
    anchor.center = (i*50+50, 50)
    anchor.physics.simulation = "manual"
    anchors.append(anchor)
    
    c = Circle((0,0),10)
    circles.append(c)
    if i==0:
        c.center = (-50, 25)
        print("setup anchor:", anchor.center, anchor.local_center, anchor.topleft)
        print("setup circle:", c.center, c.local_center, c.topleft)
    else:
        c.center = anchor.center
        print("setup anchor:", anchor.center, anchor.local_center, anchor.topleft)
        print("setup circle:", c.center, c.local_center, c.topleft)
        c.y += 100
    c.physics.simulation = "simulated"
    anchor.physics.join(c)
    add_line(anchor, c)

board.run()


