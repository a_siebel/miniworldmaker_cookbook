from miniworldmaker import *

board = PhysicsBoard((800, 600))
board.gravity = (0, 0)

a = Circle()
a.position = (75, 200)
a.color = (255,0,0)
a.impulse(90, 500)

b = Circle()
b.position = (275, 200)
b.color = (255,0,0)
b.impulse(-90, 500)

@a.register
def on_touching_circle(self, other, info):
    self.color = (0,255,0)
    print("OUTCH (a)!!!", self)
    other.border_color = (255,0,255)
    
@b.register
def on_touching_circle(self, other, info):
    self.color = (100,100,255)
    other.border_color = (255,255,255)
board.run()


