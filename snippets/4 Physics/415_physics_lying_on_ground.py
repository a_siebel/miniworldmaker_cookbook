from miniworldmaker import Rectangle, Token, Board, Line, PhysicsBoard

board = PhysicsBoard(400, 400)
board.debug = True
r = Rectangle(20, 100, 280, 100)
l = Line((0, 200), (400, 200))
board.run()