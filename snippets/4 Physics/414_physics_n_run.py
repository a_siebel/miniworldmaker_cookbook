from miniworldmaker import *

b = PhysicsBoard(400, 400)
b.boundary_x = 800
b.boundary_y = 800
#b.debug = True
class Player(Rectangle):
    def on_setup(self):
        self.on_the_ground = False
        self.direction = "right"
        self.size = (40,40)
        self.physics.size = (0.5, 0.5)
        self.physics.density = 50
        self.sensor = Sensor(self, (0, 12))
        self.sensor.size = (40, 10)
        
    def on_key_pressed_d(self):
        self.physics.velocity_x = 50
    
    def on_key_pressed_a(self):
        self.physics.velocity_x = - 50
        
    def on_key_down_w(self):
        if self.sensor.detect():
            self.physics.impulse_in_direction(0,1500)
            
    def act(self):
        self.board.camera.from_token(self)

c = Line((0,300),(400,300))
p = Player((20,270))
b.run()