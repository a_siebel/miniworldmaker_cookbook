from miniworldmaker import *

board = PhysicsBoard(600,300)
Line((0,100),(100,150))
t = Token((0,50))
t.physics.shape_type = "rect"
Line((200,100),(300,150))
t = Token((200,50))
t.physics.shape_type = "circle"
board.run()