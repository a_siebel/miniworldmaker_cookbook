from miniworldmaker import *

board = PhysicsBoard((800, 600))
board.gravity = (0, 0)

class A(Circle):
    def on_setup(self):
        self.position = (75, 200)
        self.color = (255,0,0)
        

def on_touching_token(self, other, info):
    self.color = (0,255,0)
    print("OUTCH (a)!!!", self)
    other.border_color = (255,0,255)


class B(Circle):
    def on_setup(self):
        self.position = (275, 200)
        self.color = (255,0,0)
        
    def on_touching_token(self, other, info):
        self.color = (100,100,255)
        other.border_color = (255,255,255)
    
a = A()
a.impulse(90, 500)
b = B()
b.impulse(-90, 500)
board.run()


