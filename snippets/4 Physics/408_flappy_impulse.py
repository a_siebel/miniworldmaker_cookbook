from miniworldmaker import *

board = PhysicsBoard(300, 200)

rect = Rectangle((280,120), 20, 80)
rect.physics.simulation = "manual"
ball = Circle((50,50),20)

@rect.register
def act(self):
    rect.x -= 1
    if rect.x == 0:
        rect.x = 280

@ball.register
def on_key_down(self, key):
    self.physics.impulse_in_direction(0, 5000)
board.run()