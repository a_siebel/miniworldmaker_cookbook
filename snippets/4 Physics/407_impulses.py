from miniworldmaker import *

board = PhysicsBoard((800, 600))

a = Circle()
a.position = (75, 200)
a.color = (255,0,0)
a.physics.simulation = "simulated"
a.direction = 180
a.physics.shape_type = "circle"
a.impulse(-45, 1500)
b = Circle()
b.position = (75, 200)
b.color = (255,255,0)
b.physics.simulation = "simulated"
b.physics.shape_type = "circle"
b.impulse(45, 1500)

c = Circle()
c.position = (75, 200)
c.physics.simulation = "simulated"
c.physics.shape_type = "circle"
c.color = (255,255,255)
c.impulse(135, 1500)

d = Circle()
d.position = (75, 200)
d.physics.simulation = "simulated"
d.physics.shape_type = "circle"
d.color = (0,0,0)
d.impulse(-135, 1500)

board.run()


