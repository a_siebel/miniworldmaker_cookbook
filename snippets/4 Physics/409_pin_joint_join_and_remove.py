from miniworldmaker import *
board = PhysicsBoard((400, 200))
connected = False
line = None
anchor = Rectangle()
anchor.size = (20,20)
anchor.center = (100, 20)
anchor.physics.simulation = "manual"
other_side = Line((250,100),(500,200))

def add_line(obj1, obj2):
    l = Line(obj1.center, obj2.center)
    l.physics.simulation = None
    @l.register
    def act(self):
        self.start_position = obj1.center
        self.end_position = obj2.center
    return l

c = Circle()
@c.register
def on_key_down(self, key):
    global connected
    global line
    if not connected:
        self.physics.join(anchor)
        line = add_line(self, anchor)
        connected = True
    else:
        self.physics.remove_join(anchor)
        line.remove()
        connected = False
        
board.run()


