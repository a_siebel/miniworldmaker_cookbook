import miniworldmaker
board = miniworldmaker.Board(400,400)
board.gravity = (0, 0)
p1 = miniworldmaker.Token(100,100)
# @p1.register
# def on_key_pressed_space(self):
#    print("space")
    
@p1.register
def on_key_pressed(self, keys):
    if "d" in keys:
        self.move()
        self.direction += 10
        
@p1.register
def on_key_down(self, keys):
    if "d" in keys:
        self.move()
        self.direction += 10
        
p2 = miniworldmaker.Token(300,100)

@p2.register
def on_key_pressed(self, keys):
    if "j" in keys:
        self.x = self.x + 1


board.run()