from miniworldmaker import *
import random

running = True
enemies = []

board = Board()

def setup():
    player = Circle(40,100)
    @player.register
    def on_key_pressed(self, keys):
        global running
        if running:
            if 's' in keys:
                self.y += 1
            if 'w' in keys:
                self.y -= 1
        else:
            setup()
    @player.register
    def on_detecting_token(self, other):
        if other in enemies:
            game_over()

def game_over():
    global running
    running  = False
    Text(100,100, "Game Over")
    board.stop()
    
def restart():
    global running
    enemies = []
    for token in board.tokens:
        token.remove()
    board.start()
    running = True
    setup()
    
def create_enemy():
    global enemies
    enemy = Circle(400, random.randint(0,400))
    enemies.append(enemy)
    @enemy.register
    def act(self):
        self.x -= 1
        if self.x < 0:
            enemies.remove(self)
            self.remove()
    
@board.register
def act(self):
    if self.frame % 50 == 0:
        create_enemy()

@board.register
def on_key_down(self, keys):
    global running
    if not running and "SPACE" in keys:
        restart()
        
setup()
board.run()