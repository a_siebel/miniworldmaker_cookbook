from miniworldmaker import *

board = Board()
circle = Circle((150,150), 100)
@circle.register
def on_mouse_over(self, position):
    print(position)
    self.color = (255,0,0)
@circle.register
def on_clicked_left(self, position):
    print("clicked", position)
    self.color = (255,0,255)
board.run()