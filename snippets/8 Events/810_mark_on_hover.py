from miniworldmaker import *

board = TiledBoard(10,10)
marker = None

@board.register
def on_mouse_motion(self, mouse_pos):
    global marker
    tile_pos = Position.from_pixel(mouse_pos)
    if marker:
        marker.remove()
    marker = Token(tile_pos)
    marker.add_costume((200,100,0))


        
board.run()
