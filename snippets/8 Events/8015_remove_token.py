from miniworldmaker import *

board = Board(200, 200)
print("Before token")
for event, method in board.event_manager.registered_events:
    print(event)
t = Token()
@t.register
def on_clicked_left(self, pos):
    pass
print("After creating token")
@board.register
def act(self):
    if self.frame % 20 == 0:
        t.remove()
    for event, s in board.event_manager.registered_events.items():
        if s:
            print(event, s)


            
board.run()

