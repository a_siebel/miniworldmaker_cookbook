from miniworldmaker import *

board = Board(200, 200)
class DraggableCircle(Circle):
    def on_setup(self):
        self.direction = 90
        self.dragged = False

    def on_mouse_left(self, mouse_pos):
        if self.detect_point(mouse_pos):
            self.dragged = True
        
    def on_mouse_left_released(self, mouse_pos):
        if self.dragged:
            self.dragged = False
            self.center = mouse_pos
            
drag_circle = DraggableCircle((30,30), 60)
board.run()
