from miniworldmaker import *

board = Board(120,40)
circle = Circle((20, 20))
circle.direction = 90

@circle.register
def on_mouse_left(self, mouse_pos):
    if self.detect_point(mouse_pos):
        self.move()

board.run()