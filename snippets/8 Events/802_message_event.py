from miniworldmaker import *

board = Board()

token1 = Token((2, 2))
token1.add_costume((100,0,100,100))

@token1.register
def on_message(self, message):
    print("Received message:" + message)

token2 = Token((100,100))
token2.send_message("Hello from token2")

@token2.register
def on_key_down_s(self):
    self.send_message("Hello")
board.run()


