from miniworldmaker import *

board = Board(200, 200)
circle = Circle((30, 30), 60)
circle.direction = 90
circle.dragged = False

@circle.register
def on_mouse_left(self, mouse_pos):
    print(self.detect_point(mouse_pos))
    if not self.dragged and self.detect_point(mouse_pos):
        self.dragged = True
@circle.register
def on_mouse_over(self, mouse_pos):
    if self.dragged:
        self.center = mouse_pos
        
@circle.register
def on_mouse_left_released(self, mouse_pos):
    print("released", self.dragged)
    if self.dragged:
        self.dragged = False
        self.center = mouse_pos
        
board.run()
