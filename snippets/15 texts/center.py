from miniworldmaker import *

board = Board(640, 500)

Line((320,0), (320, 500))

t1 = Text((100,100), "Test1")
print(t1.width)
t1.text = "Test1: " + str(t1.width)
print(t1.width)
t1.x = (board.width - t1.width) / 2
t1.border = 1

t2 = Text((100,150), "Test2")
t2.text = "Test2: " + str(t2.width)
t2.font_size = 50
t2.x = (board.width - t2.width) / 2
t2.border = 1

board.run()

