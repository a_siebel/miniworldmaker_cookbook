from miniworldmaker import *

board = TiledBoard(6, 4)
board.fill_color = (100, 255, 100)
board.grid = True
board.size = (10,10)
bg = board.background
@board.register
def act(self):
    board.size = (8,8)
board.run()
