from miniworldmaker import *
# not working yet
board = TiledBoard(12, 12)
board.grid = True
last_edge = None

@board.register
def on_mouse_motion(self, mouse_pos):
    global last_edge
    if last_edge:
        for token in last_edge.get_tokens():
            if hasattr(token, "marker") and token.marker:
                token.remove()
    
    edge = Edge.from_pixel(mouse_pos)
    h = edge.create_token()
    h.border = 1
    h.fill_color = (255,255,255)
    h.is_filled = True
    h.size = 0.8, 0.4
    h.direction = edge.angle
    h.marker = True
    last_edge = edge

board.run()
