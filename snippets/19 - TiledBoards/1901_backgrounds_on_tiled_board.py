import miniworldmaker

class MyBoard(miniworldmaker.TiledBoard):
    def on_setup(self):
        self.columns = 5
        self.rows = 5
        self.tile_size = 40
        self.add_background("images/soccer_green.jpg")
        print(self.background)
        token = miniworldmaker.Token()
        token.position = (3,4)
        print(token.costumes)
        print(token.costume.images)
        token.add_costume("images/player.png")
        print(token.costumes)
        print(token.costume.images)
        print(token.costume.is_upscaled)
        # Bilder von:
        # https://www.kenney.nl/assets
        
board = MyBoard(8, 6)
board.run()