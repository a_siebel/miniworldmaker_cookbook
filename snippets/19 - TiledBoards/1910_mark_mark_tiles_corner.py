from miniworldmaker import *
# not working yet
board = TiledBoard(12, 12)
board.grid = True
last_corner = None

@board.register
def on_mouse_motion(self, mouse_pos):
    global last_corner
    if last_corner:
        for token in last_corner.get_tokens():
            if hasattr(token, "marker") and token.marker:
                token.remove()
    
    corner = Corner.from_pixel(mouse_pos)
    h = corner.create_token()
    h.border = 1
    h.fill_color = (255,255,255)
    h.is_filled = True
    h.size = 0.8, 0.4
    h.marker = True
    last_corner = corner

board.run()
