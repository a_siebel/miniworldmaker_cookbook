from miniworldmaker import *

board = TiledBoard()
board.columns = 12
board.rows = 5
board.tile_size = 160
board.add_background("images/robo_green.jpg")
board.run()
