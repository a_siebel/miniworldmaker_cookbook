from miniworldmaker import *

board = TiledBoard()
board.add_background("images/soccer_green.jpg")
board.columns=20
board.rows=8
board.tile_size=40
board.background.grid = True

player = Token(position=(3, 4))
player.add_costume("images/char_blue.png")
player.border = 1
print(player.size)
print(player.position, player.direction)
print(player.image)
player.orientation = -90
@player.register
def on_key_down(self, key):
    self.move()

board.run()