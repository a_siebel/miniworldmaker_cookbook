from miniworldmaker import *
board = TiledBoard(6, 3)
board.grid = True
last_corner = None

tile = Tile((1,1))
t1 = tile.create_token()
t1.fill_color = (255,255,255)

corner = Corner((3,1), "nw")
t2 = corner.create_token()
t2.fill_color = (255,0,0)

edge = Edge((5,1), "w")
t3 = edge.create_token()
t3.fill_color = (0,0,255)
t3.size = (0.2,1)
t3.direction = edge.angle

tile=board.get_tile((1,1))
assert(tile.get_tokens()[0] == t1)

corner=board.get_corner((3,1),"nw")
assert(corner.get_tokens()[0] == t2)

edge=board.get_edge((5,1),"w")
assert(edge.get_tokens()[0] == t3)

board.run()
