from miniworldmaker import *

board = Board()
console = Console()
board.add_container(console, "bottom", size = 72)
console.pagination = True
#console.newline("test")
console.max_lines = 6

# @loop(frames = 50)
#def newline():
#    console.newline(f"newline at frame {board.frame}")
console.newline(f"newline at frame {board.frame}")
console.newline(f"newline at frame {board.frame}")
console.newline(f"newline at frame {board.frame}")
console.newline(f"newline at frame {board.frame}")
console.newline(f"newline at frame {board.frame}")
#print("add container")

@board.register
def on_key_down(self, keys):
    if ( "w" in keys):
        console.scroll_up(10)
    if ("s" in keys):
        console.scroll_down(10)
    console.pager.stick()
        


print("topleft", console.camera.topleft, console.camera.viewport_height, console.camera.boundary_y)
board.run()
