from miniworldmaker import *

board = Board()

toolbar = Toolbar()
toolbar.margin_left =  20
toolbar.margin_right = 10
toolbar.background_color = (0,0,255)



button = YesNoButton("Yes", "No")
yes = button.get_yes_button()
# yes.background_color = (0, 255, 0)
no = button.get_no_button()
# no.background_color = (255, 0, 0)
toolbar.add_widget(button)

#button2 = Button("test")
#toolbar.add_widget(button2)

board.add_container(toolbar, "right", size = 300)
@board.register
def on_message(self, message):
    print(message)

board.run()
