from miniworldmaker import *

current_board = None

def setup_board1():
    global current_board
    
    board1 = Board(500, 500)
    current_board = board1

    board1.add_background((0,255,0,255))
    print("board 1 was created")
    token = Token((10,10))
    board1.console = Console()
    board1.add_container(board1.console, "bottom", size = 200)

    @board1.register
    def act(self):
        if self.frame == 20:
            print("board 1 is running", self.frame)
            self.remove_container(self.console)
            setup_board2()
    
    return board1            
        

def setup_board2():
    board2 = Board()
    
    if current_board != board2:
        current_board.switch_board(board2)

    board2.columns = 300
    board2.rows = 300


    board2.add_background((0,0,100,255))
    token = Token((40,40))
    board2.toolbar = Toolbar()
    board2.toolbar.background_color = (255,0,0)
    board2.add_container(board2.toolbar, "right", size = 150)
    
    board2.console = Console()
    board2.console.background_color = (0,0,255)
    board2.add_container(board2.console, "bottom", size = 100)
    print("board 2 was created")
    print(board2.app.container_manager.containers)
    
    @board2.register        
    def act(self):
        if self.frame == 1:
            print("board 2 is running")
    return board2
        
        
board = setup_board1()
board.run()