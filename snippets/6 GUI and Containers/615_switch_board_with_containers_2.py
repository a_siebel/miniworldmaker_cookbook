from miniworldmaker import *

def setup_board1():
    board1 = Board()

    @board1.register
    def on_setup(self):
        self.add_background((0,255,0,255))
        print("board 1 was created")
        token = Token((10,10))
        self.console = Console()
        self.add_container(self.console, "bottom", size = 200)

    @board1.act
    def act(self):
        if self.frame == 20:
            print("board 1 is running", self.frame)
            self.remove_container(self.console)
            self.switch_board(board2)
            self.setup_board2()

def setup_board2():
    board2 = Board()
    
    @board2.register
    def on_setup(self):
        self.add_background((0,0,100,255))
        token = Token((40,40))
        self.toolbar = Toolbar()
        self.toolbar.background_color = (255,0,0)
        self.add_container(self.toolbar, "right", size = 100)
        
        self.console = Console()
        self.add_container(self.console, "bottom", size = 100)
        print("board 2 was created")
    
    @board2.register        
    def act(self):
        if self.frame == 1:
            print("board 2 is running")
        
        
        
board = Board1(400,600)
board.run()