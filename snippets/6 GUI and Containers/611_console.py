from miniworldmaker import *

board = Board()

console = Console()
board.add_container(console, "bottom")
console.newline("test")
print(console.height)

@loop(frames = 50)
def newline():
    console.newline(f"newline at frame {board.frame}")

@board.register
def on_key_down(self, keys):
    print("down, keys", keys)
    if ( "w" in keys):
        console.scroll_up(10)
    if ("s" in keys):
        console.scroll_down(10)
    print(console.camera.y)
        


board.run()
