from miniworldmaker import *

board = Board()

board.add_background("images/galaxy.jpg")

toolbar = Toolbar()
button = ToolbarButton("Start Rocket")
toolbar.add_widget(button)
board.add_container(toolbar, "right")

@board.register
def on_message(self, message):
    if message == "Start Rocket":
        rocket.started = True

rocket = Token(100, 200)
rocket.add_costume("images/ship.png")
rocket.started = False
rocket.turn_left(90)
rocket.direction = "up"

@rocket.register
def act(self):
    if self.started:
            self.move()    

@rocket.register
def on_not_detecting_board(self):
    self.remove()





board.run()
