import miniworldmaker


class MyBoard(miniworldmaker.Board):

    def __init__(self):
        super().__init__(200, 240)
        toolbar = MyToolbar()
        self.add_board("right", toolbar, 200)
        print(toolbar)
        self.rocket = Rocket(position=(100, 200))
        self.add_background("images/galaxy.jpg")

    def on_message(self, message):
        print(message)
        if message == "Start Rocket":
            self.rocket.started = True


class Rocket(miniworldmaker.Token):

    def __init__(self, position):
        super().__init__(position)
        self.add_costume("images/ship.png")
        self.started = False
        self.turn_left(90)
        self.direction = "up"

    def act(self):
        if self.started:
            self.move()

    def on_not_detecting_board(self):
        self.remove()


class MyToolbar(miniworldmaker.Toolbar):

    def __init__(self):
        super().__init__()
        button = miniworldmaker.Button("Start Rocket")
        self.add_widget(button)
        self.margin_right = 10


my_grid = MyBoard()
my_grid.run()
