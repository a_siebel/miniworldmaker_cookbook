import miniworldmaker 
from typing import Union

board = miniworldmaker.Board()

class MyButton(miniworldmaker.Widget):
    def on_clicked_left(self, mouse_pos):
        print("left")


# button_left = MyButton((40, 40), "test")

toolbar = board.add_board("right", miniworldmaker.Toolbar(), 200)
button = MyButton((0,0), "run")
button2 = MyButton((0,0), "do")
#button.set_image("images/wall.png")
print(button.position)
print(button2.position)
print(button.topleft)
print(button2.topleft)
toolbar.add_widget(button)
toolbar.add_widget(button2)
board.run()

