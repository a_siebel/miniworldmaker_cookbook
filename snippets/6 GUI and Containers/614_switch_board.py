import miniworldmaker

class Board1(miniworldmaker.PixelBoard):
    def on_setup(self):
        self.rows = 100
        self.columns = 100
        self.add_background((0,0,100,255))
        print("board 1 was created")
        self.token = miniworldmaker.Token((10,10))
        self.token.color = (255, 0, 0)
        self.token.direction = "right"
        
    def act(self):
        if self.frame < 60:
            self.token.move()
        else:
            print("board 1 is running", self.frame)
            board2 = Board2((400, 600))
            self.switch_board(board2)
            
    
class Board2(miniworldmaker.PixelBoard):
    def on_setup(self):
        self.add_background((255,255,255,255))
        self.token = miniworldmaker.Token((80,80))
        self.token.color = (0,255,0)
        #self.token.add_costume("1.png")
        #print(self.token.costume, self.token.costume.image)
        self.token.costume.set_dirty("all", 2)
        self.token.dirty = 1
        print("token created at", self.token.get_local_rect())
        print("board 2 was created")
        print(self.app.container_manager.containers)
        
    def act(self):
        if self.frame > 80:
            self.token.move()
            print("token rect", self.token.get_local_rect())
        
        
        
board = Board1(400,600)
board.run()