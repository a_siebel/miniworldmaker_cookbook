from miniworldmaker import *

board = PixelBoard(400, 400)



board2 = board.add_board("right", Board())
board2.add_background((100,200,100))

t1 = Token()
t1.board = board2


t2 = Token()
t2.add_costume((0,0,255))
t2.board = board2
t2.position = (40,40)

t3 = Token()
t3.board = board2
t3.position = (80,80)
t3.size = (10,10)

t4 = Token()
t4.add_costume("images/duck.png")
t4.board = board2
t4.position = (120,80)
t4.size = (80,80)

print(t2, t2.position_manager, t2.position, t2.board, t2.costume)

board.run()