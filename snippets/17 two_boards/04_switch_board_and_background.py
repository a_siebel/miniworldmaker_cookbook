from miniworldmaker import Board

board1 = Board()
board2 = Board()

@board1.register
def act(self):
    if self.frame == 50:
        self.switch_board(board2)

@board1.register
def on_setup(self):
    self.background = (255,255,255)

@board2.register
def on_setup(self):
    self.set_background((255,255,0))
    
board1.run()