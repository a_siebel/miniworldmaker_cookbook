from miniworldmaker import *

board = Board()
token = Token(100, 100)
board.boundary_x = 1200
board.boundary_y = 200

board.camera.x = board.camera.x + 50
board.camera.y = board.camera.y + 50

print(token.position)

@token.register
def on_clicked_left(self, pos):
    print("clicked", pos)

@token.register
def on_mouse_over(self, pos):
    print("clicked", pos)
    
board.run()