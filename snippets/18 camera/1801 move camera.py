from miniworldmaker import *

board = Board()
token = Token(100, 100)
board.boundary_x = 1200

@board.register
def act(self):
    board.camera.x += 1
    
board.run()