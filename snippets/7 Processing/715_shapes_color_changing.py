import miniworldmaker
import random
shapes = []
x = 0
board = miniworldmaker.PixelBoard((800, 600))
l = miniworldmaker.Line((0,0), (400, 300))
shapes.append(l)

r = miniworldmaker.Rectangle((400,300),400,300)
r.center = r.position
shapes.append(r)

e= miniworldmaker.Ellipse((400,300), 80, 30)
e.position = (0,0)
shapes.append(e)

c = miniworldmaker.Circle((400,300), 20)
shapes.append(c)

@board.register
def act(self):
    for shape in shapes:
        shape.color = (random.randint(0,255), random.randint(0,255), random.randint(0,255))
board.run()

