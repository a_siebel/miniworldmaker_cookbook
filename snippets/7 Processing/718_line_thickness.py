from miniworldmaker import *

board = PhysicsBoard(400,400)

for i in range(9):
    l = Line((i*40,20+i*10),((i+1)*40,20+i*10))
    l.thickness = i
    

for i in range(9):
    l = Line((i*40,80+i*10),((i+1)*40,120+i*10))
    l.thickness = i
    
for i in range(9):
    l = Line((i*40,160+i*10),((i+1)*40,160+i*10))
    l.thickness = i
    l.direction = 135


for i in range(18):
    l = Line((300,300),(350,350))
    l.direction = i * 20
    l.border_color = (i * 10, i *10, i* 10)
    
l =Line((100,300), (150,300))
@l.register
def act(self):
    l.direction+=1
    

board.debug = True
board.run()



