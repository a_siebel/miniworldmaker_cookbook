from miniworldmaker import *

shapes = []
x = 0
board = Board((800, 600))
l = Line((0,0), (400, 300))
shapes.append(l)

r = Rectangle((400,300),400,300)
r.center = r.position
shapes.append(r)

e = Ellipse((400,300), 80, 30)
e.position = (0,0)
shapes.append(e)

c = Circle((400,300), 20)
shapes.append(c)

@board.register
def act(self):
    r.width = r.width + 1
    c.radius += 1
    l.end_position = (l.end_position[0], l.end_position[1]+1)
    e.height = e.height + 1
    print(l.start_position, l.end_position, l.center, l.rect, l.size, l.direction)
board.run()

