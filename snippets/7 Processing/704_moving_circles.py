import random

from miniworldmaker import *


class MyBoard(PixelBoard):

    def on_setup(self):
        self.lst = []
        for i in range(50):
            c = Circle((random.randint(0, 800),
                                    random.randint(200, 600)),
                                    random.randint(40, 80),
                                    )
            c.border = 0
            c.fill_color=(100, 0, 255, 100)
            self.lst.append(c)
    
    def act(self):
        for circle in self.lst:
            circle.y -= (81 - circle.radius) / 10


my_board = MyBoard(800, 600)
my_board.run()
