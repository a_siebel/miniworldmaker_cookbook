import miniworldmaker

my_board = miniworldmaker.Board(800, 600)
my_board.fill_color = (255,255,255,100)
my_board.stroke_color = (0,0,0,20)

@my_board.register
def act(self):
    c = miniworldmaker.Circle(self.get_mouse_position(), 80)

@my_board.register
def on_mouse_left(self, mouse_pos):
    self.color = (200, 100, 100, 50)

@my_board.register
def on_mouse_right(self, mouse_pos):
    self.color = (255, 255, 255, 50)

my_board.run()
