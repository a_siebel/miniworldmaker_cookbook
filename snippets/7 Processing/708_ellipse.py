from miniworldmaker import *

board = PixelBoard((200, 200))
board.default_fill_color = (255,255,255,100)

Ellipse((0,100), 200, 100)

board.default_fill_color = (255,0,0,100)
Ellipse.from_center((100,100),20, 10)

board.default_fill_color = (0,255,0,100)
e = Ellipse((100,100),10, 10)
e.center = e.position

board.default_fill_color = (0,255,0,50)
e = Ellipse((100,100),18.1, 18.1)
e.center = e.position

board.run()


