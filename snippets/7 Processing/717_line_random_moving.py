from miniworldmaker import *
import random

board = Board()

line = Line((100,100),(200,100))
@line.register
def act(self):
    self.start_position = (self.start_position[0] + random.randint(-1,1), self.start_position[1]+ random.randint(-1,1))
    self.end_position = (self.end_position[0] + random.randint(-1,1), self.end_position[1]+ random.randint(-1,1))
    
board.run()
