from miniworldmaker import *


class MyBoard(PixelBoard):

    def on_setup(self):
        self.pointlist = []

    def on_mouse_left(self, pos):
        self.pointlist.append(self.get_mouse_position())

    def on_mouse_right(self, pos):
        p = Polygon(pointlist=self.pointlist)
        print(self.pointlist)
        p.color=(255, 255, 0, 255)
        self.pointlist = []



my_board = MyBoard(800, 600)
my_board.run()

