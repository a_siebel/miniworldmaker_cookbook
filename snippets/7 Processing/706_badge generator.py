import miniworldmaker

board = miniworldmaker.PixelBoard((128,128))

@board.register
def on_setup(self):
    r = miniworldmaker.Rectangle((0,0),128,128)
    t = miniworldmaker.Token((32,32))
    t.add_costume("images/cc0.png")
    t.size = (64, 64)
    t.costume.alpha = 100
    r.border = 10
    r.border_color = (0, 160, 100, 120)
    r.color = (0, 255, 100, 40)
@board.register
def on_key_down_s(self):
    print("...screenshot")
    self.screenshot()
board.run()
