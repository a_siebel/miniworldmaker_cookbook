import random

from miniworldmaker import *


class MyBoard(Board):

    def on_setup(self):
        self.add_background((255, 255, 255, 255))
        i, j = 0, 0
        distance = 30
        while i < self.width/10:
            i += 1
            x = distance * i
            y = i * distance
            self.default_fill_color = (random.randint(0, 255), random.randint(0, 255), 255, 5 * i)
            c = Circle(position=(x, y), radius=i*10)
            c.border = 0
            j = 0
            while j < self.width/10:
                j += 1
                self.default_fill_color = (random.randint(0, 255), random.randint(0, 255), 255, 5 * i)
                x, y = (distance * i) - (i * i * j), i * distance
                c = Circle((x, y), i * 10)
                c.border = 0
                x, y = i * distance, (distance * i) - (i * i * j)
                c = Circle((x, y), i * 10)
                c.border = 0

my_board = MyBoard(400, 400)
my_board.run()
