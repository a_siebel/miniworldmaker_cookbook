from miniworldmaker import *


class MyBoard(PixelBoard):

    def __init__(self):
        super().__init__(200, 200)
        self.point = 1
        self.start_pos = None
        self.end_pos = None
        l = Line((0, 0), (100, 100))
        l = color=(100, 100, 255)
        c = Circle((200, 200), 100)
        c.color = color=(255, 100, 0)
        r = Rectangle((70, 70), width=80, height=80)
        r.border = 2
        r.color=(255, 0, 0)
        print(r.width, r.height, r.rect)
        #Ellipse((500, 100), 50, 50, color=(255, 0, 0, 255), thickness=2)

    def on_mouse_left(self, mouse_position):
        if self.point == 1:
            self.start_pos = self.get_mouse_position()
            self.point = 2
        elif self.point == 2:
            self.end_pos = self.get_mouse_position()
            print(self.start_pos)
            l = Line(self.start_pos, self.end_pos)
            l.color=(255, 255, 0, 255)
            p1 = Point(self.start_pos)
            p1.color=(255, 0, 0, 255)
            p2 = Point(self.end_pos)
            p2.border = 2
            p2.color=(255, 0, 0, 255)
            print("draw line from {0} to {1}".format(self.start_pos, self.end_pos))
            self.point = 1


my_board = MyBoard()
my_board.run()
