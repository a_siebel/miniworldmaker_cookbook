import miniworldmaker

shapes = []

board = miniworldmaker.PixelBoard((800, 600))
l = miniworldmaker.Line((0,0), (400, 300))
shapes.append(l)

r = miniworldmaker.Rectangle((400,300),400,300)
r.center = r.position
shapes.append(r)

e= miniworldmaker.Ellipse((400,300), 80, 30)
e.position = (0,0)
shapes.append(e)

p = miniworldmaker.Polygon([(400,300), (400,450), (600,450)])
shapes.append(p)

@board.register
def act(self):
    for shape in shapes:
        shape.turn_right(1)
board.run()

