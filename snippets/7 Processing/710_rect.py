import miniworldmaker

board = miniworldmaker.PixelBoard((200, 200))
board.fill_color = (0,0,0)
board.default_fill_color = (255,255,255,255)

miniworldmaker.Rectangle((10,100), 180, 80)
r = miniworldmaker.Rectangle((10,100), 180, 80)
r.turn_left(45)

board.default_fill_color = (255,0,0,100)
miniworldmaker.Rectangle.from_center((100,100),20, 10)

board.default_fill_color = (0,255,0,100)
e = miniworldmaker.Rectangle((100,100),10, 10)
e.center = e.position

board.default_fill_color = (255,255,0,50)
e = miniworldmaker.Rectangle((100,100),30, 30)
e.center = e.position

board.run()


