import miniworldmaker

board = miniworldmaker.Board((200, 200))
board.default_stroke_color = (0,100,0,100)

miniworldmaker.Line((0,0), (100,0))


l = miniworldmaker.Line((0,10), (200,10))
l.border = 1

l = miniworldmaker.Line((0,0), (100,100))
l.border = 4


l = miniworldmaker.Line((50,0), (50,200))
l.border_color = (200,200,0,255)
l.border = 2

l = miniworldmaker.Line((100,0), (100,200))
l.border_color = (200,200,0,255)
l.border = 2

l = miniworldmaker.Line((100,100), (0,100))
l.border = 2


miniworldmaker.Line((50,100), (160,160))

l = miniworldmaker.Line((180,200), (100,2))
l.border_color = (200,100,0,255)
l.border = 10


#@l.register
#def act(self):
#    l.start_position = l.start_position[0], l.start_position[1]+1
#    l.end_position = l.end_position[0], l.end_position[1]+1

l2 = miniworldmaker.Line((180,220), (100,20))
l2.border_color = (100,200,0,255)
l2.border = 10

@l2.register
def act(self):
    self.y += 1
    
board.run()



