from miniworldmaker import *

board = PixelBoard((200, 200))
board.default_fill_color = (255,255,255,100)

c = Circle((100,100), 50)
assert(c.center == Position(100,100))
print(c.size)
assert(c.size==(100, 100))
board.default_fill_color = (255,0,0,100)

b = Circle.from_topleft((100,100),50)
b.direction = 10
print(b.position, b.size,)

board.run()

