from miniworldmaker import *
board = TiledBoard()
board.columns = 20
board.rows = 8
board.tile_size = 40
board.add_background("images/stone.png")
board.background.is_textured = True
t1 = Token((0,0))
t2 = Token((3,4))
t1.add_costume("images/player_blue.png")
t2.add_costume("images/player_red.png")

@t2.register
def on_mouse_left(self, mouse_pos):
    if self.detect_point(mouse_pos):
        self.dragged = True
        
@t2.register
def on_mouse_left_released(self, mouse_pos):
    if not board.is_mouse_pressed():
        self.dragged = False
        self.position = mouse_pos
        
board.run()