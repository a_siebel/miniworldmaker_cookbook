import miniworldmaker as mwm
board = mwm.TiledBoard()
board.columns = 20
board.rows = 8
board.tile_size = 40
board.add_background("images/stone.png")
board.background.is_textured = True
t1 = mwm.Token((0,0))
t2 = mwm.Token((3,4))
t1.add_costume("images/player_blue.png")
t2.add_costume("images/player_red.png")
board.run()