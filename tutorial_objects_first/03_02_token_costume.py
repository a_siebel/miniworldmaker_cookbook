import miniworldmaker as mwm

board = mwm.Board(600, 300)
board.add_background("images/grass.png")
token = mwm.Token((0, 0))
token.add_costume("images/player.png")
token = mwm.Token((100, 200))
token.add_costume("images/knight.png")
board.run()