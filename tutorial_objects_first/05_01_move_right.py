import miniworldmaker as mwm

board = mwm.Board()
board.add_background("images/grass.jpg")
player = mwm.Token()
player.add_costume("images/player.png")
player.orientation = -90 # correct image orientation
@player.register
def act(self):
    self.direction = "right"
    self.move()

board.run()