import miniworldmaker as mwm

board = mwm.Board()
board.add_background("images/grass.jpg")

t1 = mwm.Token((0,20))
t1.add_costume("images/cow.png")
t2 = mwm.Token((40,20))
t2.add_costume("images/chicken.png")
t3 = mwm.Token((80,20))
t3.add_costume("images/dog.png")
t4 = mwm.Token((120,20))
t4.add_costume("images/goat.png")

board.run()