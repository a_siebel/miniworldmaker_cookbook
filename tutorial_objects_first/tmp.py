from miniworldmaker import *

board = Board()
board.add_background("images/grass.jpg")
player = Token((90,180))
player.add_costume("images/player.png")
player.orientation = -90
@player.register
def act(self):
    player.y = player.y - 1

board.run()