from miniworldmaker import *

board = PixelBoard(400,400)
show_number = Number((100,100), 1)

@show_number.register
def on_key_down(self, key):
    n = self.get_number()
    self.set_number(n + 1)

board.run()