from miniworldmaker import *

board = TiledBoard()
board.rows = 8

def create_token(x, y):
    t = Token()
    t.position = (x,y)
    t.add_costume("images/player.png")

def create_wall(x, y):
    t = Token()
    t.position = (x,y)
    t.add_costume("images/wall.png")
    
create_token(4,2)
create_wall(4,4)
create_wall(5,4)
create_wall(6,4)
create_wall(6,3)
create_wall(6,2)
create_wall(6,1)
create_wall(5,1)
create_wall(4,1)
create_wall(3,1)

board.run()