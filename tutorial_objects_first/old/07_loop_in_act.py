from miniworldmaker import *

board = Board(100,240)
state = "green"
g = Circle ((50,40), 40)
y = Circle ((50,120), 40)
r = Circle ((50,200), 40)

@board.register
def act(self):
    global state
    if board.frame % 20 == 0:
        if state == "green":
            g.color = (0,255,0)
            y.color = (255,255,255)
            r.color = (255,255,255)
            state = "green-yellow"
        if state == "green-yellow":
            g.color = (0,255,0)
            y.color = (255,255,0)
            r.color = (255,255,255)
            state = "red"
        if state == "red":
            g.color = (255,255,255)
            y.color = (255,255,255)
            r.color = (255,0,0)
            state = "red-yellow"
        if state == "red-yellow":
            g.color = (255,255,255)
            y.color = (255,255,0)
            r.color = (255,0,0)
            state = "green"
board.run()
