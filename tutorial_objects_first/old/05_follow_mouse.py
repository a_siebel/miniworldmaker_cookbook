import miniworldmaker

board = miniworldmaker.PixelBoard()
board.columns = 400
board.rows = 400
board.add_background("images/soccer_green.jpg")
player = miniworldmaker.Token()
player.add_costume("images/player_1.png")


@player.register
def act(self):
    self.move_in_direction(self.board.get_mouse_position())

board.run()