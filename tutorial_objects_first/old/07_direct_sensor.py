from miniworldmaker import *

board = Board(200, 100)

r = Rectangle((10,10),50,100)

c = Circle((200,50),20)

@c.register
def act(self):
    self.move_left()

    
@r.register
def act(self):
    token = self.detect_token()
    print(token)
    if token:
        self.color = (255,0,0)

board.run()