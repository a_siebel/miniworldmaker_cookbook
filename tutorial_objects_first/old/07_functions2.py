from miniworldmaker import *

board = Board(400, 220)

def create_face(x, y):
    c = Circle((x, y), 40)
    c.color = (255,255,0)
    c = Circle((x + 15, y - 10), 10)
    c = Circle((x - 15, y - 10), 10)
    a = Arc((x, y + 20), 40, 20, 180, 360)
    a.center = a.position
    a.color = (255, 0, 0)
    
create_face(60, 60)
create_face(260, 60)
create_face(160, 160)

board.run()