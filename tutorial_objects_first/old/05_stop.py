from miniworldmaker import *

board = TiledBoard()
board.columns = 8
board.rows = 1
board.speed = 30
player = Token()
player.add_costume("images/player_1.png")

@player.register
def act(self):
    print(player.position)
    if player.position != (4,0):
        player.direction = "right"
        player.move()
    
    

board.run()
