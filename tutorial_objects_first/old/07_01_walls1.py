from miniworldmaker import *

board = TiledBoard()
board.columns = 8
board.rows = 2
board.speed = 30
player = Token()
player.add_costume("images/player_1.png")

wall = Token((4,0))
wall.add_costume("images/wall.png")

@player.register
def act(self):
    if player.position != (0,4):
        player.direction = "right"
        player.move()

@player.register
def on_detecting_token(self, other):
    if other==wall:
        self.move_back()
    

board.run()