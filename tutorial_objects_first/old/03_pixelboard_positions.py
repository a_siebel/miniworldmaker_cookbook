import miniworldmaker

board = miniworldmaker.PixelBoard()
board.columns = 800
board.rows = 400

miniworldmaker.Token((0,0))
miniworldmaker.Token((200,100))
miniworldmaker.Token((300,60))
board.run()
