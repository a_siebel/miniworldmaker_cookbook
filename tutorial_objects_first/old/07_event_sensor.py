from miniworldmaker import *

board = Board(200, 100)

r = Rectangle((10,10),50,100)

c1 = Circle((200,50),20)
c2 = Circle((120,50),20)

@c1.register
def act(self):
    self.move_left()

@c2.register
def act(self):
    self.move_left()
    
@r.register
def on_detecting_tokens(self, other):
    if other == c1:
        self.color = (255,0,0)
    if other == c2:
        self.color = (0, 255,0)

board.run()