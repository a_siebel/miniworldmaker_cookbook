from miniworldmaker import *

board = Board(80,80)

robot =  Token()
robot.size = (80,80)
robot.add_costume("images/drive1.png")
robot.costume.add_image("images/drive2.png")
robot.costume.is_animated = True
robot.costume.loop = True
board.run()