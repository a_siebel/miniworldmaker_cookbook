import miniworldmaker as mwm

board = mwm.Board()
board.add_background("images/grass.png")
player = mwm.Token()
player.add_costume("images/player.png")
player.position = (100,200)
board.run()
