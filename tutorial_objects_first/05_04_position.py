import miniworldmaker as mwm
import random

board = mwm.Board(400, 400)
board.add_background("images/grass.jpg")
player = mwm.Token((100, 100))
player.add_costume("images/target.png")
player.orientation = -90

@player.register
def act(self):
    if self.board.frame % 50 == 0: # every 50th frame:
        player.position = (random.randint(0, 400), random.randint(0, 400))

board.run()