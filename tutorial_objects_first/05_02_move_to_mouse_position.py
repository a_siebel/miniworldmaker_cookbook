import miniworldmaker as mwm

board = mwm.Board(400, 400)
board.add_background("images/grass.jpg")
player = mwm.Token()
player.add_costume("images/player.png")
player.orientation = -90

@player.register
def act(self):
    self.move_in_direction(self.board.get_mouse_position())

board.run()