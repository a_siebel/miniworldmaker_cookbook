import miniworldmaker as mwm

board = mwm.Board(400,400)
board.add_background("images/grass.jpg")
player = mwm.Token()
player.add_costume("images/player.png")
player.orientation = -90
player.position = (200, 200)

@player.register
def act(self):
    self.direction = self.board.frame
    self.move()
    

board.run()