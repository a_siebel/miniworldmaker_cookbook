This repository contains examples for miniworldmaker ( [miniworldmaker.de](miniworldmaker.de) ), a 2D engine based on pygame.

### Directories:

  * *tests*: Contains small snippets. These can be helpful to understand how certain aspects can be realized in miniworldmaker.

  * *objects_first*: Contains examples where **no** classes are used.

  * *classes_first*: Contains examples in which classes are used.

  * *tutorial*: Examples from the tutorial from miniworldmaker.com.


### Videos

Miniworldmaker in 15 Minutes:

  * [1](https://www.youtube.com/watch?v=Ah1sR-x8O9U&t=5s) 

  * [2](https://www.youtube.com/watch?v=8rNDO8ec-y0&t=2s)
  
  * [3](https://www.youtube.com/watch?v=oWQiKni9Zuo&t=1s)